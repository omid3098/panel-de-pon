using MessagePack;

namespace PanelDePon
{
    [MessagePackObject]
    public class BoardData
    {
        [Key(0)] public SlotData[,] slotsData { get; set; }
    }
}