using DG.Tweening;
using TMPro;
using UnityEngine;

namespace PanelDePon
{
    public class DifficultyMenuItem : MenuItem
    {
        [SerializeField] TextMeshPro textComponent = null;
        [SerializeField] Transform leftRadioPos = null;
        [SerializeField] Transform rightRadioPos = null;
        [SerializeField] Transform radioCircleButton = null;
        [SerializeField] Shaker shaker = null;
        [SerializeField] SpriteRenderer backgroundCSpriteRenderer = null;
        [SerializeField] Color casualBackgroundColor = Color.white;
        private TextAlignmentOptions textAlighnment;
        private GameModeManager modeManager;
        private Vector3 originPosition;
        private bool moving;
        private static string lastGameModeKey = "LAST_GAME_MODE_KEY";
        private void Start()
        {
            textAlighnment = new TextAlignmentOptions();
            modeManager = GameModeManager.Instance;
            SetMode(GameModes.classic);
        }

        private void SetMode(GameModes gamemode)
        {
            moving = true;
            modeManager.SetMode(gamemode);
            shaker.Shake();
            UpdateView(gamemode);
        }

        private void UpdateView(GameModes gamemode)
        {
            if (gamemode == GameModes.casual)
            {
                textComponent.text = "Casual";
                AudioManager.Instance.Play(AudioNames.CasualMenu, true);
                AudioManager.Instance.Stop(AudioNames.CasualGame);
                AudioManager.Instance.Stop(AudioNames.ClassicGame);
                AudioManager.Instance.Stop(AudioNames.ClassicMenu);
                textComponent.alignment = TextAlignmentOptions.MidlineLeft;
                backgroundCSpriteRenderer.color = casualBackgroundColor;
                radioCircleButton.transform.SetParent(rightRadioPos);
            }
            else
            {
                textComponent.text = "Classic";
                AudioManager.Instance.Stop(AudioNames.CasualMenu);
                AudioManager.Instance.Stop(AudioNames.CasualGame);
                AudioManager.Instance.Stop(AudioNames.ClassicGame);
                AudioManager.Instance.Play(AudioNames.ClassicMenu, true);
                textComponent.alignment = TextAlignmentOptions.MidlineRight;
                backgroundCSpriteRenderer.color = Color.white;
                radioCircleButton.transform.SetParent(leftRadioPos);
            }
            var moveTween = radioCircleButton.DOLocalMove(Vector3.zero, 0.1f);
            moveTween.SetEase(Ease.OutQuart);
            moveTween.onComplete += () =>
            {
                moving = false;
            };
        }

        public override void Select()
        {
            if (moving == false)
            {
                if (modeManager.currentSetting.gameMode == GameModes.casual)
                {
                    SetMode(GameModes.classic);
                }
                else
                {
                    SetMode(GameModes.casual);
                }
            }
        }
    }
}