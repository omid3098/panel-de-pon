using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace PanelDePon
{
    public class Shaker : MonoBehaviour
    {
        [SerializeField] float shake_speed = 15f;
        [SerializeField] float shake_intensity = 10f;
        [SerializeField] float shake_duration = 0.1f;
        Vector3 originPosition = Vector3.zero;
        public bool busy { get; private set; }

        void Start()
        {
            originPosition = transform.position;
        }

        void Update()
        {
            if (busy)
            {
                float step = shake_speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, originPosition + UnityEngine.Random.insideUnitSphere, step);
            }
        }
        public void Shake()
        {
            busy = true;
            StartCoroutine(StopShake());
        }

        private IEnumerator StopShake()
        {
            yield return new WaitForSeconds(shake_duration);
            var tween = transform.DOMove(originPosition, 0.2f);
            tween.SetEase(Ease.OutQuart);
            tween.onComplete += () =>
            {
                busy = false;
            };
        }
    }
}