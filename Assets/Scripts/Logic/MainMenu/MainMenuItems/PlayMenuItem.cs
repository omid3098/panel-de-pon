using UnityEngine;
namespace PanelDePon
{
    public class PlayMenuItem : MenuItem
    {
        [SerializeField] int sceneIndexToLoad = 0;
        public override void Select()
        {
            Debug.Log("Play");
            TransitionManager.Instance.LoadScene(sceneIndexToLoad);
            // SceneManager.LoadScene(sceneIndexToLoad);
        }
    }
}