using UnityEngine;

namespace PanelDePon
{
    public class ExitMenuItem : MenuItem
    {
        public override void Select()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}