﻿using UnityEngine;
namespace PanelDePon
{
    public abstract class MenuItem : MonoBehaviour
    {
        protected BaseMenuManager menuManager;
        public abstract void Select();

        internal void SetMenuManager(BaseMenuManager baseMenuItemSelector)
        {
            this.menuManager = baseMenuItemSelector;
        }
    }
}