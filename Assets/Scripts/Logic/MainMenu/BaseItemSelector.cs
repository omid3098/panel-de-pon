using System.Collections;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

namespace PanelDePon
{
    public abstract class BaseMenuManager : MonoBehaviour
    {
        [ReorderableList] [SerializeField] protected MenuItem[] menuItems = null;
        [SerializeField] protected int currentSelectedIndex = 1;
        [SerializeField] protected Vector3 selectedScale = new Vector3(1.3f, 1.3f, 1);
        private Vector3 startScale = Vector3.zero;
        protected IPlayer player = null;
        protected virtual void Awake()
        {
            player = GameInputManager.Instance.GetPlayer(0);
            foreach (var menuItem in menuItems)
            {
                menuItem.SetMenuManager(this);
            }
        }
        protected virtual void OnEnable()
        {
            // currentSelectedIndex = 1;
            StartListening();
        }

        public void StartListening()
        {
            player.Active();
            player.Up += SelectUp;
            player.Down += SelectDown;
            player.Left += SelectDown;
            player.Right += SelectUp;
            player.A += ExecuteSelectedItem;
            SelectMenuItem();
            // StartCoroutine(MakeSurePlayerActiveCoroutine());
        }

        // IEnumerator MakeSurePlayerActiveCoroutine()
        // {
        //     yield return new WaitForSeconds(1f);
        //     if (player.active == false)
        //     {
        //         Debug.Log("Activating player after 1 second");
        //         player.Active();
        //     }
        // }

        protected void SelectMenuItem()
        {
            // reset back all menu item scales
            foreach (var menuItem in menuItems)
            {
                if (startScale == Vector3.zero) startScale = menuItem.transform.localScale;
                menuItem.transform.localScale = startScale;
            }

            AudioManager.Instance.Play(AudioNames.moveCursor);
            // scale up selected menu item;
            Transform menuItemTransform = menuItems[currentSelectedIndex].transform;
            player.MoveCursorToParent(menuItemTransform);
            var scaleTween = menuItemTransform.DOScale(selectedScale, 0.2f);
            scaleTween.SetEase(Ease.OutElastic);
            scaleTween.onComplete += () =>
            {
                for (int i = 0; i < menuItems.Length; i++)
                {
                    MenuItem menuItem = menuItems[i];
                    if (currentSelectedIndex != i) menuItem.transform.localScale = startScale;
                }
            };
        }
        protected virtual void OnDisable()
        {
            player.Deactive();
            StopListening();
        }

        public void StopListening()
        {
            player.Up -= SelectUp;
            player.Down -= SelectDown;
            player.Left -= SelectDown;
            player.Right -= SelectUp;
            player.A -= ExecuteSelectedItem;
        }

        private void SelectDown(int playerIndex)
        {
            currentSelectedIndex++;
            currentSelectedIndex = currentSelectedIndex % menuItems.Length;
            SelectMenuItem();
        }

        private void SelectUp(int playerIndex)
        {
            currentSelectedIndex -= 1;
            if (currentSelectedIndex < 0) currentSelectedIndex = menuItems.Length - 1;
            SelectMenuItem();
        }
        private void ExecuteSelectedItem(int playerIndex)
        {
            AudioManager.Instance.Play(AudioNames.chain);
            menuItems[currentSelectedIndex].Select();
        }
    }
}