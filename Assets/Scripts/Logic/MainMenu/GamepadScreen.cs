using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace PanelDePon
{
    public class GamepadScreen : MonoBehaviour
    {
        [SerializeField] float waitTime = 2f;
        [SerializeField] SpriteRenderer spriteRenderer = null;
        [SerializeField] PressAnyKey pressAnyKey = null;
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(waitTime);
            spriteRenderer.DOFade(0, 0.4f);
            pressAnyKey.Active();
        }
    }
}