using UnityEngine;
using UnityEngine.InputSystem;

namespace PanelDePon
{
    public class AnyDeviceKey
    {
        public static bool WasPressed()
        {
            if (Input.anyKey) return true;
            if (Gamepad.current == null) return false;
            else return (
                Gamepad.current.xButton.wasPressedThisFrame ||
                Gamepad.current.aButton.wasPressedThisFrame ||
                Gamepad.current.bButton.wasPressedThisFrame ||
                Gamepad.current.yButton.wasPressedThisFrame ||
                Gamepad.current.startButton.wasPressedThisFrame ||
                Gamepad.current.selectButton.wasPressedThisFrame);
        }
    }
    public class PressAnyKey : MonoBehaviour
    {
        [SerializeField] int sceneIndex;
        [SerializeField] bool active;

        private void Update()
        {
            if (active)
            {
                if (AnyDeviceKey.WasPressed())
                    TransitionManager.Instance.LoadScene(sceneIndex);
            }
            // SceneManager.LoadScene(sceneIndex);
        }

        internal void Active()
        {
            active = true;
        }
    }
}