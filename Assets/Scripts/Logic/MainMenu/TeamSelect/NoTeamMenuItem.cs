namespace PanelDePon
{
    public class NoTeamMenuItem : TeamMenuItem
    {
        public override void Select()
        {
            TeamManager.RemovePlayerFromAllTeams(player);
        }
    }
}