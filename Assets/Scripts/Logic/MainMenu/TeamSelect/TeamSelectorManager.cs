using UnityEngine;
using UnityEngine.SceneManagement;

namespace PanelDePon
{
    public class TeamSelectorManager : MonoBehaviour
    {
        [SerializeField] TeamSelector[] teamSelectors = null;
        [SerializeField] int sceneIndexToLoad = 0;
        private void OnEnable()
        {
            TeamManager.Initialize(2);
            foreach (var teamSelector in teamSelectors) teamSelector.OnPlayerReady += OnPlayerReady;
        }
        private void OnDisable()
        {
            foreach (var teamSelector in teamSelectors) teamSelector.OnPlayerReady -= OnPlayerReady;
        }

        private void OnPlayerReady()
        {
            bool allReady = true;
            foreach (var teamSelector in teamSelectors)
            {
                if (teamSelector.player != null && teamSelector.player.active && teamSelector.ready == false)
                {
                    allReady = false;
                    break;
                }
            }
            if (allReady)
            {
                TransitionManager.Instance.LoadScene(sceneIndexToLoad);
                // SceneManager.LoadScene(sceneIndexToLoad);
            }
        }
    }
}