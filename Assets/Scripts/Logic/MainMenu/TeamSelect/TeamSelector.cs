using NaughtyAttributes;
using UnityEngine;
namespace PanelDePon
{
    public class TeamSelector : MonoBehaviour
    {
        [ReorderableList] [SerializeField] TeamMenuItem[] teamMenuItems = null;
        [SerializeField] int playerIndex = 0;
        [SerializeField] Transform playerButton = null;
        [SerializeField] TMPro.TextMeshPro playerName = null;
        public IPlayer player { get; private set; }
        private int currentSelectedIndex;
        public bool ready { get; private set; }
        public event VoidDelegate OnPlayerReady;
        private void Awake()
        {
            player = GameInputManager.Instance.GetPlayer(playerIndex);
        }

        private void OnEnable()
        {
            if (player != null)
            {
                if (playerIndex == 0)
                {
                    ActivePlayerAndStartListening(0);
                    // Debug.Log("Player is acrivated: " + playerIndex);
                }
                else
                {
                    player.Start += ActivePlayerAndStartListening;
                    // Debug.Log("Player is waiting for activation: " + playerIndex);
                }
                foreach (var teamMenuItem in teamMenuItems) teamMenuItem.SetPlayer(player);
            }
        }

        private void ActivePlayerAndStartListening(int playerIndex)
        {
            if (player != null)
            {
                Debug.Log("Activating player " + playerIndex);
                player.MoveCursorToParent(playerButton);
                currentSelectedIndex = 1;
                MovePlayerButtonToNewParent();
                if (player.active == false)
                {
                    player.Active();
                    player.Left += Left;
                    player.Right += Right;
                    player.A += Select;
                }
                else
                {
                    player.Deactive();
                    player.Left -= Left;
                    player.Right -= Right;
                    player.A -= Select;
                }
            }
        }

        private void OnDisable()
        {
            if (player != null)
            {
                Debug.Log("Removing listeners for player " + playerIndex);
                player.Deactive();
                player.Start -= ActivePlayerAndStartListening;
                player.Left -= Left;
                player.Right -= Right;
                player.A -= Select;
            }
        }

        private void MovePlayerButtonToNewParent()
        {
            playerButton.transform.SetParent(teamMenuItems[currentSelectedIndex].transform, false);
            if (currentSelectedIndex == 1)
            {
                ready = false;
                playerName.text = "P" + playerIndex;
            }
        }

        private void Select(int playerIndex)
        {
            var team = teamMenuItems[currentSelectedIndex];
            team.Select();
            // if the index doea not point to the middle item [0,1,2] we have selected a team and we can set ready
            if (currentSelectedIndex != 1)
            {
                ready = true;
                playerName.text = "Ready!";
                if (OnPlayerReady != null) OnPlayerReady.Invoke();
            }
            else
            {
                ready = false;
                playerName.text = "P" + playerIndex;
            }
        }

        private void Right(int playerIndex)
        {
            Debug.Log("Player Right pressed: " + playerIndex);
            currentSelectedIndex++;
            if (currentSelectedIndex > 2) currentSelectedIndex = 2;
            MovePlayerButtonToNewParent();
        }

        private void Left(int playerIndex)
        {
            Debug.Log("Player Left pressed: " + playerIndex);
            currentSelectedIndex--;
            if (currentSelectedIndex < 0) currentSelectedIndex = 0;
            MovePlayerButtonToNewParent();
        }
    }
}