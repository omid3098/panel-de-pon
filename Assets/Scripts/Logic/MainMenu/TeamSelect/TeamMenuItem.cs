using UnityEngine;

namespace PanelDePon
{
    public abstract class TeamMenuItem : MenuItem
    {
        protected IPlayer player;
        public void SetPlayer(IPlayer player)
        {
            this.player = player;
        }
    }
}