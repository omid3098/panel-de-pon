/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using UnityEngine;

namespace PanelDePon
{
    public class Board : IBoard
    {
        private TileBuilder tilebuilder;
        public ISlotManager slotManager { get; private set; }

        private int teamIndex;
        public event AttackDelegate Attack;
        public event BoardDelegate GameOver;
        ICombo combo { get; set; }
        public Board(ISlotManager slotManager, ICombo combo, int team)
        {
            // TODO: Slotmanager must have players not board!
            tilebuilder = Singleton<TileBuilder>.Instance;
            tilebuilder.Initialize();
            this.slotManager = slotManager;
            this.teamIndex = team;
            this.combo = combo;

            slotManager.GameOver += OnGameOver;
            slotManager.Combo += OnCombo;
            slotManager.Chain += OnChain;
        }


        public void FillBoard(BoardData boardData = null)
        {
            if (boardData != null)
            {
                // / tile builder bayayd in board data ro bede? ya haminja ba board data bayad besazim? ehtemalan haminja
                int widthCount = boardData.slotsData.GetLength(0);
                int heightCount = boardData.slotsData.GetLength(1);
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        SlotData slotData = boardData.slotsData[i, j];
                        if (slotData.tileData != null)
                        {
                            slotManager.Fill(i, j, tilebuilder.GetTile(slotData.tileData, teamIndex));
                        }
                    }
                }
            }
            else
            {
                // fill half of the board with tiles
                for (int i = 0; i < slotManager.heightCount / 2; i++)
                {
                    slotManager.FillRow(i);
                }
            }
            slotManager.CheckMatches();
        }

        private void OnChain(Transform highestSlot, int chainValue)
        {
            combo.ShowChain(highestSlot, chainValue);
            if (Attack != null) Attack(this, chainValue, AttackType.chain);
        }

        private void OnCombo(Transform transform, int value)
        {
            combo.ShowCombo(transform, value);
            if (Attack != null) Attack(this, value, AttackType.combo);
        }

        private void OnGameOver()
        {
            if (GameOver != null) GameOver.Invoke(this);
        }

        // private void UpdateCursor()
        // {
        //     foreach (var player in players)
        //     {
        //     }
        // }

        public void Update()
        {
            slotManager.OnUpdate();
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                DropGarbage(3, AttackType.combo);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                DropGarbage(4, AttackType.combo);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                DropGarbage(6, AttackType.combo);
            }
        }

        public void DropGarbage(int attackStrenght, AttackType attackType)
        {
            int strenght = attackStrenght;
            // chain 3 should make a garbage with strenght 2
            if (attackType == AttackType.chain) strenght -= 1;
            ITile[] garbageTile = tilebuilder.GetGarbage(strenght, attackType, teamIndex);
            if (garbageTile != null) slotManager.ReceiveGarbageAttack(garbageTile);
        }
    }
}
