using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;

namespace PanelDePon
{
    public class AddresseblaManager<T>
    {
        struct AddressablePair
        {
            public string address;
            public T asset;
        }
        private static List<AddressablePair> cachedAddressablePairs = new List<AddressablePair>();
        public static async Task<T> Load(string address)
        {
            T result = cachedAddressablePairs.Find(x => x.address == address).asset;
            if (result == null)
            {
                UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<T> asyncOperationHandle = Addressables.LoadAssetAsync<T>(address);
                await asyncOperationHandle.Task;
                var pair = new AddressablePair() { address = "address", asset = result };
                cachedAddressablePairs.Add(pair);
                result = asyncOperationHandle.Result;
            }
            return result;
        }
    }
}
