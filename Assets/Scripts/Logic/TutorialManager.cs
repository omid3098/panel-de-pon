/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Lean.Pool;
using UnityEngine;
namespace PanelDePon
{
    public class TutorialManager : MonoBehaviour
    {
        [SerializeField] List<GameObject> tutorialPrefabs = new List<GameObject>();
        [SerializeField] GameObject button;
        [SerializeField] TMPro.TextMeshPro buttonTextComponent;
        [SerializeField] int sceneIndex;
        GameObject currentTutorialObject;
        private bool started;
        int currentIndex = 0;
        private IPlayer player;
        private TweenerCore<Vector3, Vector3, VectorOptions> tween;

        private void OnEnable()
        {
            if (player == null) player = GameInputManager.Instance.GetPlayer(0);
            if (player.active == false) player.Active();
            player.A += APressed;
            StartTutorial();
        }
        public void OnDisable()
        {
            player.A -= APressed;
        }

        private void APressed(int value)
        {
            if (started) Next();
        }

        public void StartTutorial()
        {
            Debug.Log("Starting tutorial");
            started = true;
            currentIndex = 0;
            Play();
            player.MoveCursorToParent(button.transform);
            buttonTextComponent.text = "Next";
        }

        void Play()
        {
            Debug.Log("Current tutorial index: " + currentIndex);
            if (currentIndex == tutorialPrefabs.Count - 1)
                buttonTextComponent.text = "Ok!";
            if (currentTutorialObject != null) Destroy(currentTutorialObject);
            currentTutorialObject = GenerateTutorialObject(currentIndex);
            var startScale = currentTutorialObject.transform.localScale;
            currentTutorialObject.transform.localScale = Vector3.zero;
            tween = currentTutorialObject.transform.DOScale(startScale, 0.3f);
            tween.SetEase(Ease.OutElastic);
        }

        public void Next()
        {
            if (tween == null || tween.IsPlaying() == false)
            {
                Debug.Log("Next!" + currentIndex);
                currentIndex++;
                if (currentIndex >= tutorialPrefabs.Count) CloseTheLastOne();
                else Play();
            }
        }

        private void CloseTheLastOne()
        {
            LeanPool.Despawn(currentTutorialObject);
            started = false;
            TransitionManager.Instance.LoadScene(sceneIndex);
        }

        private GameObject GenerateTutorialObject(int index)
        {
            var tutPrefab = LeanPool.Spawn(tutorialPrefabs[index]);
            return tutPrefab;
        }
    }
}