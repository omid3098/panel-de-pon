namespace PanelDePon
{
    public class AudioNames
    {
        public static string chain = "chainAudio";
        public static string moveCursor = "moveCursorAudio";
        public static string swap = "swapAudio";
        public static string tilePop = "tilePopAudio";
        public static string bigChain1 = "bigChain1";
        public static string bigChain2 = "bigChain2";
        public static string bigChain3 = "bigChain3";
        public static string gameover = "gameoverAudio";
        public static string dropGarbage = "garbageDropAudio";
        public static string countDownAudio = "countDown";
        public static string lastCountDownAudio = "lastCountDown";
        public static string CasualMenu = "CasualMenu";
        public static string CasualGame = "CasualGame";
        public static string ClassicMenu = "ClassicMenu";
        public static string ClassicGame = "ClassicGame";
    }
}
