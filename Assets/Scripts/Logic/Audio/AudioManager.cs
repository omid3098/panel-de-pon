﻿using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
namespace PanelDePon
{
    [System.Serializable]
    public struct AudioClipData
    {
        public string audioName;
        public AudioClip audioClip;
    }
    public class AudioManager : Singleton<AudioManager>
    {
        [ReorderableList] [SerializeField] List<AudioClipData> audioClipDatas = null;
        struct AudioSourcePair
        {
            public string audioName;
            public AudioSource source;
        }
        private List<AudioSourcePair> cachedAudioPairs = new List<AudioSourcePair>();
        private GameObject poolGameObject;
        public void Play(string audioName, bool loop = false)
        {
            AudioSource source = cachedAudioPairs.Find(x => x.audioName == audioName).source;
            if (source == null)
            {
                // Load audioClip into cachedAudioPairs and play it when load complete
                if (poolGameObject == null)
                {
                    poolGameObject = new GameObject("AudioManagerPool");
                    GameObject.DontDestroyOnLoad(poolGameObject);
                }
                var clipDataIndex = audioClipDatas.FindIndex(x => x.audioName == audioName);
                if (clipDataIndex != -1)
                {
                    source = poolGameObject.AddComponent<AudioSource>();
                    source.playOnAwake = false;
                    source.clip = audioClipDatas[clipDataIndex].audioClip;
                    cachedAudioPairs.Add(new AudioSourcePair()
                    {
                        audioName = audioName,
                        source = source
                    });
                    source.loop = loop;
                    source.Play();
                }
                else
                {
                    Debug.LogError("Counld not find audio with clipname: " + audioName);
                }
            }
            else
            {
                source.loop = loop;
                source.Play();
            }
        }

        internal void Stop(string audioName)
        {
            AudioSource source = cachedAudioPairs.Find(x => x.audioName == audioName).source;
            if (source != null)
            {
                source.Stop();
            }
        }
    }
}
