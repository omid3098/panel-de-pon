using UnityEngine;
using UnityEngine.InputSystem;

namespace KeyBinding
{
    public class GamePadInput : MonoBehaviour
    {
        void FixedUpdate()
        {
            var gamepad = Gamepad.current;
            if (gamepad == null)
                return; // No gamepad connected.
            Vector2 move = gamepad.leftStick.ReadValue();

            if (gamepad.rightTrigger.wasPressedThisFrame)
            {
                Debug.Log("Right trigger pressed!");
                // 'Use' code here
            }
            else if (gamepad.dpad.up.wasPressedThisFrame) Debug.Log("Up");
            else if (gamepad.dpad.down.wasPressedThisFrame) Debug.Log("down");
            if (move != Vector2.zero)
            {
                // 'Move' code here
                Debug.Log(move);
            }
        }
    }
}
