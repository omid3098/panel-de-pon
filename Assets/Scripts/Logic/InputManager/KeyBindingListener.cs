﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputActionRebindingExtensions;

namespace KeyBinding
{
    [System.Serializable]
    public class PlayerInputActionProfile
    {
        public string profileName;
        public InputAction axis;
        // public InputAction up;
        // public InputAction down;
        // public InputAction left;
        // public InputAction right;
        public InputAction start;
        public InputAction select;
        public InputAction a;
        public InputAction b;
        public InputAction x;
        public InputAction y;
        public InputAction l1;
        public InputAction r1;
        public InputAction[] allInputActions { get; private set; }
        public PlayerInputActionProfile(int playerIndex)
        {
            profileName = "Player" + playerIndex;
            axis = new InputAction("axis", InputActionType.Value, "2DVector");
            // up = new InputAction("up", InputActionType.Button, "2DVector");
            // down = new InputAction("down", InputActionType.Button, "2DVector");
            // left = new InputAction("left", InputActionType.Button, "2DVector");
            // right = new InputAction("right", InputActionType.Button, "2DVector");
            start = new InputAction("start", InputActionType.Button);
            select = new InputAction("select", InputActionType.Button);
            a = new InputAction("a", InputActionType.Button);
            b = new InputAction("b", InputActionType.Button);
            x = new InputAction("x", InputActionType.Button);
            y = new InputAction("y", InputActionType.Button);
            l1 = new InputAction("l1", InputActionType.Button);
            r1 = new InputAction("r1", InputActionType.Button);

            allInputActions = new InputAction[] { axis
            // ,start, select, a, b, x, y, l1, r1 
            };
        }
    }
    public class KeyBindingListener : MonoBehaviour
    {
        PlayerInputActionProfile playerProfile = null;
        int bindIndex = 0;
        private void Awake()
        {
            Debug.Log("Wellcome to player input config.");
            // playerProfile = new PlayerInputActionProfile(1);
            // StartBingings(bindIndex);

            var myAction = new InputAction(binding: "/*/<button>");
            myAction.performed += (context) => Debug.Log($"Button {context.control.name} pressed!");
            myAction.Enable();
        }

        private void StartBingings(int index)
        {
            InputAction inputAction = playerProfile.allInputActions[index];
            Debug.Log("Press any key for " + inputAction.name + " ...");
            var rebindOperation = inputAction.PerformInteractiveRebinding()
                    // To avoid accidental input from mouse motion
                    .WithControlsExcluding("Mouse")
                    .WithRebindAddingNewBinding()
                    .OnMatchWaitForAnother(0.1f)
                    // .WithExpectedControlType(typeof(Vector2))
                    .Start();
            rebindOperation.OnComplete(RemapComplete);
        }

        private void RemapComplete(RebindingOperation rebindOperation)
        {
            Debug.Log(rebindOperation.action.name + " set!");
            rebindOperation.Dispose();
            bindIndex++;
            if (bindIndex < playerProfile.allInputActions.Length)
            {
                StartBingings(bindIndex);
            }
            else
            {
                Debug.Log("Finished! profile activated. Press any configured keys to see them working");
                for (int i = 0; i < playerProfile.allInputActions.Length; i++)
                {
                    InputAction inputAction = playerProfile.allInputActions[i];
                    inputAction.Enable();
                    inputAction.performed += InputActionPressed;
                }
            }
        }

        private void InputActionPressed(InputAction.CallbackContext context)
        {
            Debug.Log(context.action.name + " was pressed! " + context.ReadValue<Vector2>());
        }

        // private void OnEnable()
        // {
        //     InputSystem.onEvent += TrackEvents;
        // }

        // private void TrackEvents(InputEventPtr inputEvent, InputDevice inputDevice)
        // {
        //     Debug.Log(inputDevice.displayName + " - " + inputEvent);
        //     var deviceID = inputDevice.id;
        // }

        // private void OnDisable()
        // {
        //     InputSystem.onEvent -= TrackEvents;
        // }
    }
}
