/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using UnityEngine;

namespace PanelDePon
{
    [System.Serializable]
    public class PlayerInputEvent
    {
        public KeyCode[] keys;
        public event VoidDelegate Pressed;
        public event VoidDelegate Released;
        internal void Update()
        {
            foreach (var key in keys)
            {
                if (Input.GetKeyDown(key))
                {
                    if (Pressed != null) Pressed.Invoke();
                }
                if (Input.GetKeyUp(key))
                {
                    if (Released != null) Released.Invoke();
                }
            }
        }
    }
    [CreateAssetMenu]
    public class PlayerInput : ScriptableObject
    {
        public PlayerInputEvent Left;
        public PlayerInputEvent Right;
        public PlayerInputEvent Up;
        public PlayerInputEvent Down;
        public PlayerInputEvent A;
        public PlayerInputEvent B;
        public PlayerInputEvent X;
        public PlayerInputEvent Y;
        public PlayerInputEvent Start;
        public PlayerInputEvent Select;
        public PlayerInputEvent Trigger;
        internal void Update()
        {
            Up.Update();
            Down.Update();
            Left.Update();
            Right.Update();
            A.Update();
            B.Update();
            X.Update();
            Y.Update();
            Start.Update();
            Select.Update();
            Trigger.Update();
        }
    }
}