/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

namespace PanelDePon
{
    public interface IInputManager
    {
        void Tick();
        event VoidDelegate Left;
        event VoidDelegate Right;
        event VoidDelegate Up;
        event VoidDelegate Down;
        event VoidDelegate A;
        event VoidDelegate B;
        event VoidDelegate X;
        event VoidDelegate Y;
        event VoidDelegate Start;
        event VoidDelegate Back;
        event VoidDelegate TriggerPressed;
        event VoidDelegate TriggerReleased;
    }
}