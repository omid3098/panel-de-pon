﻿/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */
namespace PanelDePon
{
    public class KeyboardInputManager : IInputManager
    {
        private PlayerInput input;
        public event VoidDelegate Left;
        public event VoidDelegate Right;
        public event VoidDelegate Up;
        public event VoidDelegate Down;
        public event VoidDelegate A;
        public event VoidDelegate B;
        public event VoidDelegate X;
        public event VoidDelegate Y;
        public event VoidDelegate Start;
        public event VoidDelegate Back;
        public event VoidDelegate TriggerPressed;
        public event VoidDelegate TriggerReleased;


        public KeyboardInputManager(PlayerInput input)
        {
            this.input = input;
            input.Up.Pressed += UpPressed;
            input.Down.Pressed += DownPressed;
            input.Left.Pressed += LeftPressed;
            input.Right.Pressed += RightPressed;
            input.A.Pressed += APressed;
            input.B.Pressed += BPressed;
            input.X.Pressed += XPressed;
            input.Y.Pressed += YPressed;
            input.Start.Pressed += StartPressed;
            input.Select.Pressed += SelectPressed;
            input.Trigger.Pressed += TriggerPress;
            input.Trigger.Released += TriggerRelease;
        }
        public void Dispose()
        {
            input.Up.Pressed -= UpPressed;
            input.Down.Pressed -= DownPressed;
            input.Left.Pressed -= LeftPressed;
            input.Right.Pressed -= RightPressed;
            input.A.Pressed -= APressed;
            input.B.Pressed -= BPressed;
            input.X.Pressed -= XPressed;
            input.Y.Pressed -= YPressed;
            input.Start.Pressed -= StartPressed;
            input.Select.Pressed -= SelectPressed;
            input.Trigger.Pressed -= TriggerPress;
            input.Trigger.Released -= TriggerRelease;
            this.input = null;
        }

        private void UpPressed() { if (Up != null) Up.Invoke(); }
        private void DownPressed() { if (Down != null) Down.Invoke(); }
        private void LeftPressed() { if (Left != null) Left.Invoke(); }
        private void RightPressed() { if (Right != null) Right.Invoke(); }
        private void APressed() { if (A != null) A.Invoke(); }
        private void BPressed() { if (B != null) B.Invoke(); }
        private void XPressed() { if (X != null) X.Invoke(); }
        private void YPressed() { if (Y != null) Y.Invoke(); }
        private void StartPressed() { if (Start != null) Start.Invoke(); }
        private void SelectPressed() { if (Back != null) Back.Invoke(); }
        private void TriggerPress() { if (TriggerPressed != null) TriggerPressed.Invoke(); }
        private void TriggerRelease() { if (TriggerReleased != null) TriggerReleased.Invoke(); }

        public void Tick()
        {
            input.Update();
        }
    }
}
