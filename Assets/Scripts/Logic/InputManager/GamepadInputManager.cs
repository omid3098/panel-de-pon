using UnityEngine.InputSystem;

namespace PanelDePon
{
    public class GamepadInputManager : IInputManager
    {
        private Gamepad gamepad;

        public GamepadInputManager(int i)
        {
            gamepad = Gamepad.all[i];
        }

        public event VoidDelegate Left;
        public event VoidDelegate Right;
        public event VoidDelegate Up;
        public event VoidDelegate Down;
        public event VoidDelegate A;
        public event VoidDelegate B;
        public event VoidDelegate X;
        public event VoidDelegate Y;
        public event VoidDelegate Start;
        public event VoidDelegate Back;
        public event VoidDelegate TriggerPressed;
        public event VoidDelegate TriggerReleased;

        public void Initialize() { }
        public void Tick()
        {
            if (gamepad.dpad.left.wasPressedThisFrame) if (Left != null) Left.Invoke();
            if (gamepad.dpad.right.wasPressedThisFrame) if (Right != null) Right.Invoke();
            if (gamepad.dpad.up.wasPressedThisFrame) if (Up != null) Up.Invoke();
            if (gamepad.dpad.down.wasPressedThisFrame) if (Down != null) Down.Invoke();
            if (gamepad.buttonSouth.wasPressedThisFrame) if (A != null) A.Invoke();
            if (gamepad.buttonEast.wasPressedThisFrame) if (B != null) B.Invoke();
            if (gamepad.buttonWest.wasPressedThisFrame) if (X != null) X.Invoke();
            if (gamepad.buttonNorth.wasPressedThisFrame) if (Y != null) Y.Invoke();
            if (gamepad.startButton.wasPressedThisFrame) if (Start != null) Start.Invoke();
            if (gamepad.selectButton.wasPressedThisFrame) if (Back != null) Back.Invoke();
            if (gamepad.leftShoulder.wasPressedThisFrame || gamepad.rightShoulder.wasPressedThisFrame) if (TriggerPressed != null) TriggerPressed.Invoke();
            if (gamepad.leftShoulder.wasReleasedThisFrame || gamepad.rightShoulder.wasReleasedThisFrame) if (TriggerReleased != null) TriggerReleased.Invoke();

            // if (gamepad.dpad.left.wasPressedThisFrame) if (OnLeft != null) OnLeft.Invoke();
            // if (gamepad.dpad.right.wasPressedThisFrame) if (OnRight != null) OnRight.Invoke();
            // if (gamepad.dpad.up.wasPressedThisFrame) if (OnUp != null) OnUp.Invoke();
            // if (gamepad.dpad.down.wasPressedThisFrame) if (OnDown != null) OnDown.Invoke();
            // if (gamepad.buttonSouth.wasPressedThisFrame) if (A != null) A.Invoke();
            // if (gamepad.buttonEast.wasPressedThisFrame) if (OnBack != null) OnBack.Invoke();
            // if (gamepad.leftShoulder.wasPressedThisFrame || gamepad.rightShoulder.wasPressedThisFrame) if (TriggerPressed != null) TriggerPressed.Invoke();
            // if (gamepad.leftShoulder.wasReleasedThisFrame || gamepad.rightShoulder.wasReleasedThisFrame) if (OnSlow != null) OnSlow.Invoke();
        }
    }
}
