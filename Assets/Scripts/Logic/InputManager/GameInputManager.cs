﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.InputSystem;

namespace PanelDePon
{
    public class GameInputManager : Singleton<GameInputManager>
    {
        private const int maxPlayerCount = 4;
        [SerializeField] PlayerInput p1KeyboardInput = null;
        [SerializeField] PlayerInput p2KeyboardInput = null;
        [ReorderableList] [SerializeField] GameObject[] playerCursorPrefabs = null;
        // public static IInputManager[] playersInputManager;
        public static IPlayer[] players { get; private set; }
        protected override void Awake()
        {
            base.Awake();
            players = new IPlayer[maxPlayerCount];

            players[0] = new Player(0, new KeyboardInputManager(p1KeyboardInput), new BasicCursor(playerCursorPrefabs[0]));
            players[1] = new Player(1, new KeyboardInputManager(p2KeyboardInput), new BasicCursor(playerCursorPrefabs[1]));

            Debug.Log("Found " + Gamepad.all.Count + " gamepads!");
            for (int i = 0; i < Gamepad.all.Count; i++)
            {
                IPlayer player = players[i];
                if (player == null)
                {
                    players[i] = new Player(i, new GamepadInputManager(i), new BasicCursor(playerCursorPrefabs[i]));
                    Debug.Log("Assign player " + i + " to gamepad");
                }
                else
                {
                    player.AddInput(new GamepadInputManager(i));
                }
            }
        }

        private void Update()
        {
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i] != null) players[i].Tick();
            }
        }

        internal IPlayer GetPlayer(int playerIndex)
        {
            return players[playerIndex];
        }
    }
}
