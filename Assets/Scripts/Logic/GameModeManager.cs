/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */
using UnityEngine;
namespace PanelDePon
{
    public enum GameModes
    {
        casual,
        classic
    }
    public class GameModeManager : Singleton<GameModeManager>
    {
        [SerializeField] GameModeSetting classicMode = null;
        [SerializeField] GameModeSetting easyMode = null;
        public GameModeSetting currentSetting { get; private set; }

        private BoardConfigManager boardConfigManager;

        public event VoidDelegate SettingChanged;
        protected override void Awake()
        {
            base.Awake();
            currentSetting = easyMode;
        }
        public void SetMode(GameModes mode)
        {
            if (boardConfigManager == null) boardConfigManager = BoardConfigManager.Instance;
            switch (mode)
            {
                case GameModes.casual:
                    currentSetting = easyMode;
                    boardConfigManager.SetWidth(8);
                    break;
                case GameModes.classic:
                    currentSetting = classicMode;
                    boardConfigManager.SetWidth(6);
                    break;
                default:
                    Debug.LogError("Unknown game mode passed!");
                    break;
            }
            if (SettingChanged != null) SettingChanged.Invoke();
        }
    }
}