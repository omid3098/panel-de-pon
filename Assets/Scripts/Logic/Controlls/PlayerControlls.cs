// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Logic/Controlls/PlayerControlls.inputactions'

using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class PlayerControlls : IInputActionCollection
{
    private InputActionAsset asset;
    public PlayerControlls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerControlls"",
    ""maps"": [
        {
            ""name"": ""Player2"",
            ""id"": ""1d4191ae-2fc2-4c6f-884b-90e27137118e"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""e0c84e97-17d3-4fa2-8de8-b914e1ba69e7"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Swap"",
                    ""type"": ""Button"",
                    ""id"": ""4bf799f1-7b53-4ddb-b0c0-09cbcc14d45e"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fast"",
                    ""type"": ""Button"",
                    ""id"": ""92542ffe-9138-4212-80fd-99d00920932d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Arrow"",
                    ""id"": ""5c2f3b3b-dd82-4d20-bf21-71bd3006517c"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""ac2eccaa-7552-4aa0-9de5-d77fd65b9335"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""91700a30-73fb-40d7-b8a3-2650bc769d91"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""3e35adad-db7b-41de-83f7-509c787c1034"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""d73eda7b-cafa-40d4-8427-b8337d1436b5"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""gpad"",
                    ""id"": ""6423b5d1-acf7-4303-a333-14204df0ef46"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""d9b162e0-8be4-4c60-93aa-20d9daed3610"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""b8a7f998-1b57-4836-8af5-03a457dbf9fd"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""a04559b5-ed86-4e73-ae10-11ae1bd1e0b6"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""eae7c83a-d088-4e2c-b1de-a5031325b9ec"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""7a5c50da-7de7-49bd-8353-42b230a79d1d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""925290a0-ff47-4158-a4e7-017e9151a9e8"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Thumb2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e47b0f68-b28e-4c21-9ee3-de984cc5a6c6"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Thumb"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5b28a31c-8b7b-4555-a40a-6ff23a0c7221"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3027555-1667-4ad2-9901-76f12cfa30e6"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Top2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ba1f6d2c-38dc-4179-adb6-003798cb2a15"",
                    ""path"": ""<Linux::SinoLiteTechnologyCorp::MobileCASSY>/Pinkie"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Player1"",
            ""id"": ""732ec17d-4a7b-4cc5-b671-9f0764da44af"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""a6d506a6-0a85-47a5-9457-29d67fc78cec"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Swap"",
                    ""type"": ""Button"",
                    ""id"": ""cdbd483e-1efe-4dec-9740-9b592c68636c"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fast"",
                    ""type"": ""Button"",
                    ""id"": ""8333374f-5ea9-40ed-b5a6-942a10408e66"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""bc310beb-87cc-4e64-bee1-11bef5974f8b"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""99417321-2a78-462a-bed9-3428bdfb57a1"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""d57a0fa5-ef4b-478d-8176-6ed279f67dc1"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""5cdd95fa-1382-4601-9478-f8bc87921d23"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""53100ce3-344b-4e09-a6d9-6e76653c7a3c"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""gpad"",
                    ""id"": ""136eed57-1cc7-49c0-95c9-4c9f1d98d19d"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""6f31c38f-9831-44cf-8a85-988c9c6ced0f"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Stick/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""da7051c6-6e68-4d48-bdba-d6040984c78d"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Stick/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""a1edf556-2236-4d1e-9032-a68713541bda"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Stick/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""8afada21-8b0f-4317-ba6a-db76c6dc1566"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Stick/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Gamepad"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""8c45a106-3847-4284-a4bd-5eb6485c53ff"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": "";Keyboard"",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""eb05f9cc-ff68-4732-a6ae-2869a6e6208f"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Thumb2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1af3022a-aebe-4f8d-98e4-61268957348d"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Thumb"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Swap"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""1f99b0f0-e3c3-4b5c-aadf-534babf9db0e"",
                    ""path"": ""<Keyboard>/leftCtrl"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4e7cc08c-ee32-49d1-9cab-639876f22a04"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Top2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""538d22b5-4c5c-4026-a62d-ce0f77b6dd5f"",
                    ""path"": ""<Linux::PersonalCommunicationSystemsInc::TwinUSBGamepad>/Pinkie"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fast"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Player2
        m_Player2 = asset.GetActionMap("Player2");
        m_Player2_Movement = m_Player2.GetAction("Movement");
        m_Player2_Swap = m_Player2.GetAction("Swap");
        m_Player2_Fast = m_Player2.GetAction("Fast");
        // Player1
        m_Player1 = asset.GetActionMap("Player1");
        m_Player1_Movement = m_Player1.GetAction("Movement");
        m_Player1_Swap = m_Player1.GetAction("Swap");
        m_Player1_Fast = m_Player1.GetAction("Fast");
    }

    ~PlayerControlls()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player2
    private readonly InputActionMap m_Player2;
    private IPlayer2Actions m_Player2ActionsCallbackInterface;
    private readonly InputAction m_Player2_Movement;
    private readonly InputAction m_Player2_Swap;
    private readonly InputAction m_Player2_Fast;
    public struct Player2Actions
    {
        private PlayerControlls m_Wrapper;
        public Player2Actions(PlayerControlls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Player2_Movement;
        public InputAction @Swap => m_Wrapper.m_Player2_Swap;
        public InputAction @Fast => m_Wrapper.m_Player2_Fast;
        public InputActionMap Get() { return m_Wrapper.m_Player2; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player2Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer2Actions instance)
        {
            if (m_Wrapper.m_Player2ActionsCallbackInterface != null)
            {
                Movement.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnMovement;
                Swap.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnSwap;
                Swap.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnSwap;
                Swap.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnSwap;
                Fast.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnFast;
                Fast.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnFast;
                Fast.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnFast;
            }
            m_Wrapper.m_Player2ActionsCallbackInterface = instance;
            if (instance != null)
            {
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                Swap.started += instance.OnSwap;
                Swap.performed += instance.OnSwap;
                Swap.canceled += instance.OnSwap;
                Fast.started += instance.OnFast;
                Fast.performed += instance.OnFast;
                Fast.canceled += instance.OnFast;
            }
        }
    }
    public Player2Actions @Player2 => new Player2Actions(this);

    // Player1
    private readonly InputActionMap m_Player1;
    private IPlayer1Actions m_Player1ActionsCallbackInterface;
    private readonly InputAction m_Player1_Movement;
    private readonly InputAction m_Player1_Swap;
    private readonly InputAction m_Player1_Fast;
    public struct Player1Actions
    {
        private PlayerControlls m_Wrapper;
        public Player1Actions(PlayerControlls wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Player1_Movement;
        public InputAction @Swap => m_Wrapper.m_Player1_Swap;
        public InputAction @Fast => m_Wrapper.m_Player1_Fast;
        public InputActionMap Get() { return m_Wrapper.m_Player1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player1Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer1Actions instance)
        {
            if (m_Wrapper.m_Player1ActionsCallbackInterface != null)
            {
                Movement.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnMovement;
                Swap.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnSwap;
                Swap.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnSwap;
                Swap.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnSwap;
                Fast.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnFast;
                Fast.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnFast;
                Fast.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnFast;
            }
            m_Wrapper.m_Player1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                Swap.started += instance.OnSwap;
                Swap.performed += instance.OnSwap;
                Swap.canceled += instance.OnSwap;
                Fast.started += instance.OnFast;
                Fast.performed += instance.OnFast;
                Fast.canceled += instance.OnFast;
            }
        }
    }
    public Player1Actions @Player1 => new Player1Actions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.GetControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface IPlayer2Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnSwap(InputAction.CallbackContext context);
        void OnFast(InputAction.CallbackContext context);
    }
    public interface IPlayer1Actions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnSwap(InputAction.CallbackContext context);
        void OnFast(InputAction.CallbackContext context);
    }
}
