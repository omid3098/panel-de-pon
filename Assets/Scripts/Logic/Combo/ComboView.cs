
/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
/*
 * File Created: Tuesday, 20th August 2019 12:43:21 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Tuesday, 20th August 2019 12:43:30 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using DG.Tweening;
using TMPro;
using UnityEngine;

namespace PanelDePon
{
    public class ComboView : MonoBehaviour
    {
        [SerializeField] TextMeshPro textComponent = null;
        private const float scaleFactor = 1.5f;
        private const float tweenDuration = 1f;
        public void SetValue(string value)
        {
            textComponent.text = value;
            var tween = transform.DOScale(new Vector3(scaleFactor, scaleFactor, scaleFactor), tweenDuration);
            var moveTween = transform.DOMoveY(20, tweenDuration);
            tween.SetRelative(true);
            tween.SetEase(Ease.OutQuart);
            moveTween.SetRelative(true);
            moveTween.SetEase(Ease.OutQuart);
        }
    }
}