
/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
/*
 * File Created: Tuesday, 20th August 2019 12:43:21 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Tuesday, 20th August 2019 12:43:30 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using DG.Tweening;
using UnityEngine;

namespace PanelDePon
{
    public class ComboBehaviour : MonoBehaviour, ICombo
    {
        [SerializeField] ComboView comboViewPrefab = null;
        [SerializeField] ComboView chainViewPrefab = null;

        public void ShowChain(Transform highestSlot, int chainValue)
        {
            var chainObject = Instantiate(chainViewPrefab);
            chainObject.transform.position = highestSlot.position - new Vector3(0f, 0.5f);
            chainObject.SetValue("x" + chainValue);
            var tween = chainObject.transform.DOMoveY(0.7f, 1f);
            tween.SetRelative(true);
            tween.SetEase(Ease.OutQuart);
            tween.onComplete += () => { chainObject.gameObject.SetActive(false); };
        }

        public void ShowCombo(Transform slotTransform, int value)
        {
            var comboObject = Instantiate(comboViewPrefab);
            comboObject.transform.position = slotTransform.position;
            comboObject.SetValue(value.ToString());
            var tween = comboObject.transform.DOMoveY(0.7f, 1f);
            tween.SetRelative(true);
            tween.SetEase(Ease.OutQuart);
            tween.onComplete += () => { comboObject.gameObject.SetActive(false); };
        }
    }

    public interface ICombo
    {
        void ShowCombo(Transform transform, int value);
        void ShowChain(Transform highestSlot, int chainValue);
    }
}