using System;

namespace PanelDePon
{
    public class TeamManager
    {
        public static Team[] teams { get; private set; }
        public static void Initialize(int teamCount)
        {
            if (teams != null) Array.Clear(teams, 0, teams.Length);
            teams = new Team[teamCount];
            for (int i = 0; i < teamCount; i++)
            {
                teams[i] = new Team(i);
            }
        }

        internal static void RemovePlayerFromAllTeams(IPlayer player)
        {
            for (int i = 0; i < teams.Length; i++)
            {
                teams[i].RemovePlayer(player);
            }
        }

        public static void AddPlayer(int teamIndex, IPlayer player)
        {
            teams[teamIndex].AddPlayer(player);
        }

        public static void RemovePlayer(int teamIndex, IPlayer player)
        {
            teams[teamIndex].RemovePlayer(player);
        }

        internal static int GetTeamsWithPlayerCount()
        {
            int teamCount = 0;
            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i].players.Count > 0) teamCount++;
            }
            return teamCount;
        }
    }
}
