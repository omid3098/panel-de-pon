using System.Collections.Generic;
using UnityEngine;
namespace PanelDePon
{
    public class Player : IPlayer
    {
        public ICursor cursor { get; private set; }
        public int playerIndex { get; private set; }
        public bool active { get; private set; }
        List<IInputManager> inputManagers;
        public event IntDelegate Left;
        public event IntDelegate Right;
        public event IntDelegate Up;
        public event IntDelegate Down;
        public event IntDelegate A;
        // public event IntDelegate B;
        // public event IntDelegate X;
        // public event IntDelegate Y;
        public event IntDelegate Start;
        public event IntDelegate Back;
        public event IntDelegate TriggerPressed;
        public event IntDelegate TriggerReleased;

        public Player(int playerIndex, IInputManager inputManager, ICursor cursor)
        {
            inputManagers = new List<IInputManager>();
            this.playerIndex = playerIndex;
            this.cursor = cursor;
            AddInput(inputManager);
        }

        public void Active()
        {
            // Debug.Log("Playar activated");
            active = true;
            cursor.Active();
        }

        public void Deactive()
        {
            active = false;
            cursor.Deactive();
        }
        private void AddInputListeners()
        {
            foreach (var inputManager in inputManagers)
            {
                inputManager.Left += LeftPressed;
                inputManager.Right += RightPressed;
                inputManager.Up += UpPressed;
                inputManager.Down += DownPressed;
                inputManager.A += APress;
                inputManager.TriggerPressed += TriggerPress;
                inputManager.TriggerReleased += TriggerRelease;
                inputManager.Back += BackPressed;
                inputManager.Start += StartPressed;
            }
        }
        void RemoveInputListeners()
        {
            foreach (var inputManager in inputManagers)
            {
                inputManager.Left -= LeftPressed;
                inputManager.Right -= RightPressed;
                inputManager.Up -= UpPressed;
                inputManager.Down -= DownPressed;
                inputManager.A -= APress;
                inputManager.TriggerPressed -= TriggerPress;
                inputManager.TriggerReleased -= TriggerRelease;
                inputManager.Back -= BackPressed;
                inputManager.Start -= StartPressed;
            }
        }

        private void StartPressed() { if (Start != null) Start.Invoke(playerIndex); }
        private void LeftPressed() { if (Left != null) Left.Invoke(playerIndex); }
        private void RightPressed() { if (Right != null) Right.Invoke(playerIndex); }
        private void UpPressed() { if (Up != null) Up.Invoke(playerIndex); }
        private void DownPressed() { if (Down != null) Down.Invoke(playerIndex); }
        private void BackPressed() { if (Back != null) Back.Invoke(playerIndex); }
        private void APress() { if (A != null) A.Invoke(playerIndex); }
        private void TriggerPress() { if (TriggerPressed != null) TriggerPressed.Invoke(playerIndex); }
        private void TriggerRelease() { if (TriggerReleased != null) TriggerReleased.Invoke(playerIndex); }
        public void Tick()
        {
            foreach (var inputManager in inputManagers) inputManager.Tick();
        }

        public void AddInput(IInputManager inputmanager)
        {
            if (inputManagers.Contains(inputmanager) == false)
            {
                RemoveInputListeners();
                inputManagers.Add(inputmanager);
                AddInputListeners();
            }
        }
        public void MoveCursorToParent(Transform viewTransform) => cursor.UpdateView(viewTransform);
    }
}
