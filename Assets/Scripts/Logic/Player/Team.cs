using System.Collections.Generic;

namespace PanelDePon
{
    public class Team
    {
        public int index;
        public List<IPlayer> players { get; private set; }
        public Team(int index)
        {
            this.index = index;
            players = new List<IPlayer>();
        }
        public void AddPlayer(IPlayer player)
        {
            var exist = players.FindIndex(x => x.playerIndex == player.playerIndex);
            if (exist == -1) players.Add(player);
        }
        public void RemovePlayer(IPlayer player)
        {
            var index = players.FindIndex(x => x.playerIndex == player.playerIndex);
            if (index != -1) players.RemoveAt(index);
        }
    }
}
