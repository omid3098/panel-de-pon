
using UnityEngine;

namespace PanelDePon
{
    public interface IPlayer
    {
        event IntDelegate Left;
        event IntDelegate Right;
        event IntDelegate Up;
        event IntDelegate Down;
        event IntDelegate A;
        // event IntDelegate B;
        // event IntDelegate X;
        // event IntDelegate Y;
        event IntDelegate Start;
        event IntDelegate Back;
        event IntDelegate TriggerPressed;
        event IntDelegate TriggerReleased;
        int playerIndex { get; }
        bool active { get; }
        void Active();
        void Deactive();
        void Tick();
        void AddInput(IInputManager inputmanager);
        void MoveCursorToParent(Transform viewTransform);
        ICursor cursor { get; }
    }
}