using MessagePack;

namespace PanelDePon
{
    [MessagePackObject]
    public class SlotData
    {
        [Key(0)] public int x { get; set; }
        [Key(1)] public int y { get; set; }
        [Key(2)] public TileData tileData { get; set; }
    }
}