﻿using System;

namespace PanelDePon
{
    public interface ISlotManager
    {
        void Initialize(int teamIndex, IPlayer[] players);
        event VoidDelegate GameOver;
        event ComboDelegate Combo;
        event ComboDelegate Chain;
        int widthCount { get; }
        int heightCount { get; }
        void CheckMatches();
        void Swap(ISlot currentSlot, ISlot nextSlot, float duration, bool checkCompletion = true);
        void OnUpdate();
        void ReceiveGarbageAttack(ITile[] garbageTile);
        void FillRow(int rowIndex);
        void Fill(int x, int y, ITile tile);
    }
}