using System;
using System.Threading.Tasks;
using UnityEngine;

namespace PanelDePon
{
    public class CheckMatch : ICheckMatch
    {
        private const int matchArrayCount = 5;
        private ISlot[,] slots;
        private int teamIndex;
        private ISlot[] matchSlots;
        private int widthCount;
        private int heightCount;
        private int chainCounter;
        private GameModeSetting setting;
        private TileBuilder tileBuilder;
        private int comboCounter;

        public event ComboDelegate Combo;
        public event ComboDelegate Chain;
        public event BoolDelegate Complete;
        public event VoidDelegate MatchFound;
        public static int lastID { get; private set; }
        public int currentID { get; private set; }
        public CheckMatch(ISlot[,] slots, int teamIndex)
        {
            setting = Singleton<GameModeManager>.Instance.currentSetting;
            tileBuilder = Singleton<TileBuilder>.Instance;
            lastID++;
            currentID = lastID;
            this.slots = slots;
            this.teamIndex = teamIndex;
            widthCount = slots.GetLength(0);
            heightCount = slots.GetLength(1);
            matchSlots = new ISlot[matchArrayCount];
            chainCounter = 0;
        }

        // /*
        //   ██████╗██╗  ██╗███████╗ ██████╗██╗  ██╗    ███╗   ███╗ █████╗ ████████╗ ██████╗██╗  ██╗
        //  ██╔════╝██║  ██║██╔════╝██╔════╝██║ ██╔╝    ████╗ ████║██╔══██╗╚══██╔══╝██╔════╝██║  ██║
        //  ██║     ███████║█████╗  ██║     █████╔╝     ██╔████╔██║███████║   ██║   ██║     ███████║
        //  ██║     ██╔══██║██╔══╝  ██║     ██╔═██╗     ██║╚██╔╝██║██╔══██║   ██║   ██║     ██╔══██║
        //  ╚██████╗██║  ██║███████╗╚██████╗██║  ██╗    ██║ ╚═╝ ██║██║  ██║   ██║   ╚██████╗██║  ██║
        //   ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝    ╚═╝     ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝╚═╝  ╚═╝
        // */

        public async void Execute()
        {
            // checking horizontal and vertical matches
            comboCounter = CheckMatchInDimention(true) + CheckMatchInDimention(false);
            // if we have any results execute match effect for them
            if (comboCounter > 0)
            {
                if (MatchFound != null) MatchFound.Invoke();
                // check for neighbour slots and mark them as matched if necessary
                CheckGarbageNeighbours();
                // using last slot to add bind listener to its completion

                // using higest matched slot for showing combo
                int highestSlotX = 0;
                int highestSlotY = 0;
                // find the highest slots to invoke combo and chain effects
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        ISlot slot = slots[i, j];
                        if (slot.matchID == currentID)
                        {
                            if (i > highestSlotX) highestSlotX = i;
                            if (j > highestSlotY) highestSlotY = j;
                            // for the last one, add listener
                        }
                    }
                }

                // Combo logic
                Transform highestSlotTransform = slots[highestSlotX, highestSlotY].slotObject.transform;
                if (comboCounter > 3) if (Combo != null) Combo.Invoke(highestSlotTransform, comboCounter);

                // Chain logic:
                chainCounter++;
                if (chainCounter > 1) if (Chain != null) Chain.Invoke(highestSlotTransform, chainCounter);

                var executionWaitTask = Task.Delay(TimeSpan.FromSeconds(setting.ExecutionWaitAfterMatch));
                await executionWaitTask;

                // then execute match process, one by one
                // first for non garbage tiles
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        var slot = slots[i, j];
                        if (slot.matchID == currentID && slot.hasGarbage == false)
                        {
                            var executionTask = Task.Delay(TimeSpan.FromSeconds(setting.SingleTileDestroyWait));
                            await executionTask;
                            slot.ExecuteMatch();
                        }
                    }
                }
                // then for garbage tiles 
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        var mainGarbageSlot = slots[i, j];
                        if (mainGarbageSlot.hasTile == true && mainGarbageSlot.matchID == currentID && mainGarbageSlot.hasGarbage == true && mainGarbageSlot.recyclable == false)
                        {
                            var garbageWidth = mainGarbageSlot.GetTile().data.width;
                            var garbageHeight = mainGarbageSlot.GetTile().data.height;
                            mainGarbageSlot.Freez();

                            // dont execute the main garbage, just frees it and execute match after replacing complete
                            mainGarbageSlot.ExecuteMatch();

                            // if garbage width is higher that 1, make a new garbage with the remaining width and assign to top slot if this slot
                            var newHeight = garbageHeight - 1;
                            if (newHeight > 0)
                            {
                                var newGarbage = tileBuilder.GetGarbage(newHeight, AttackType.chain, teamIndex);
                                ISlot topSlot = slots[i, j + 1];
                                topSlot.SetTile(newGarbage[0]);
                                topSlot.Freez();
                            }

                            var executionTask = Task.Delay(TimeSpan.FromSeconds(setting.SingleTileDestroyWait));
                            await executionTask;
                            // Unlock first row
                            for (int l = 0; l < garbageWidth; l++)
                            {
                                ISlot garbageSlot = slots[i + l, j];
                                var recycledTile = tileBuilder.GetRandomNormalTile();
                                garbageSlot.RemoveGarbage();
                                garbageSlot.SetTile(recycledTile);
                                garbageSlot.MarkRecycle();
                                garbageSlot.Freez();
                                Task waitTask = Task.Delay(TimeSpan.FromSeconds(setting.RecycleDurationPerTile));
                                await waitTask;
                            }
                        }
                    }
                }
                // remove recycle and frozen state for slots
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        var slot = slots[i, j];
                        if (slot.recyclable && slot.frozen)
                        {
                            slot.RemoveRecyclable();
                            slot.UnFreez();
                        }
                    }
                }
                // await one more in destroy time then complete the task
                var lastWait = Task.Delay(TimeSpan.FromSeconds(setting.SingleTileDestroyWait));
                await lastWait;
                if (Complete != null) Complete.Invoke(true);
            }
            else
            {
                chainCounter = 0;
                // no more matches, complete with false result so we dont update airbornes after this
                if (Complete != null) Complete.Invoke(false);
            }
        }

        private void CheckGarbageNeighbours()
        {
            // TODO: for concrete garbages that should break linking dependency, we need to check neightbour tiletype to be normal
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    var slot = slots[i, j];
                    if (slot.hasGarbage)
                    {
                        // check all 4 dimentions for neighbour marked for match tiles
                        bool topMatched = false;
                        bool botMatched = false;
                        bool leftMatched = false;
                        bool rightMatched = false;
                        if (j < heightCount - 1) topMatched = slots[i, j + 1].matchID != -1;
                        if (j > 0) botMatched = botMatched = slots[i, j - 1].matchID != -1;
                        if (i > 0) leftMatched = slots[i - 1, j].matchID != -1;
                        if (i < widthCount - 1) rightMatched = slots[i + 1, j].matchID != -1;
                        if (topMatched || botMatched || leftMatched || rightMatched)
                        {
                            if (slot.matchID == -1)
                            {
                                slot.MarkMatch(currentID);
                                // retrace from the begining to mark all neighbour garbages as matched
                                // because each garbaged slot does not recognize its neighbout garbage slots
                                i = 0;
                                j = 0;
                            }
                        }
                    }
                }
            }
        }

        int CheckMatchInDimention(bool horizontal)
        {
            int result = 0;
            int matchedCounter = 0;
            int width = horizontal ? heightCount : widthCount;
            int height = horizontal ? widthCount : heightCount;
            for (int i = 0; i < width; i++)
            {
                // at the begining of each row, proccess array for matches on the previous row
                ProcessMatchedArray(ref matchedCounter, ref result);
                for (int j = 0; j < height; j++)
                {
                    ISlot currentSlot = horizontal ? slots[j, i] : slots[i, j];
                    ITile tile = currentSlot.GetTile();
                    // if slot has a normal tile and its not frozen if slot is not airborned
                    if (currentSlot.airBornDistance == 0 &&                                     // if slot is not airborned
                        tile != null &&                                                         // if slot has a tile
                        tile.data.tileType == TileTypes.NormalTileType &&                       // if slot has a normal tile type
                        currentSlot.frozen == false &&                                          // if slot is not frozen
                        (currentSlot.matchID == -1 || currentSlot.matchID == currentID))        // if the slot is not marked for match or is matched with current ID
                    {
                        // if matched counter is 0, we only need to fill the first one with current tile
                        if (matchedCounter == 0)
                        {
                            matchSlots[matchedCounter] = currentSlot; matchedCounter++;
                        }
                        else
                        {
                            // compare the current type with first tile type and if they match, add current slot to the array
                            if (matchSlots[0].tileName == currentSlot.tileName)
                            {
                                matchSlots[matchedCounter] = currentSlot; matchedCounter++;
                            }
                            // if they dont match, process matched array to check if we have more than 3 matches and clean the array 
                            else
                            {
                                // process current matched slots, clean match slots and reset match counter
                                ProcessMatchedArray(ref matchedCounter, ref result);
                                // then fill the first matched slot with current slot for the next loop checks
                                matchSlots[matchedCounter] = currentSlot; matchedCounter++;
                            }
                        }
                    }
                    else                                                                        // if current slot is not valid for check, process array so far and clean variables
                    {
                        ProcessMatchedArray(ref matchedCounter, ref result);
                    }
                }
            }
            return result;
        }
        void ProcessMatchedArray(ref int matchedCounter, ref int result)
        {
            // if we have a sequence of the same type, mark them
            if (matchedCounter >= 3)
            {
                for (int m = 0; m < matchedCounter; m++)
                {
                    matchSlots[m].MarkMatch(currentID);
                    result++;
                }
                // Debug.Log("Matched " + matchedCounter + " tiles with color" + matchSlots[0].tileName);
            }
            matchedCounter = 0;
            Array.Clear(matchSlots, 0, matchArrayCount);
        }
    }

    public interface ICheckMatch
    {
        int currentID { get; }
        void Execute();
        event ComboDelegate Combo;
        event ComboDelegate Chain;
        event BoolDelegate Complete;
        event VoidDelegate MatchFound;
    }
}
