using UnityEngine;
namespace PanelDePon
{
    public class BasicSlot : MonoBehaviour, ISlot
    {
        private void Awake()
        {
            data = new SlotData();
            matchID = -1;
        }
        public GameObject slotObject => gameObject;
        ITile currentTile { get; set; }
        private bool tileMoving;
        public bool swapLock => tileMoving == true || hasGarbage == true || airBornDistance > 0 || matchID != -1 || frozen == true;
        public bool hasTile => currentTile != null;
        public int matchID { get; private set; }
        public bool hasGarbage { get; private set; }
        public bool recyclable { get; private set; }
        public int airBornDistance { get; private set; }
        public string tileName => data.tileData.tileName;
        public SlotData data { get; private set; }
        public bool frozen { get; private set; }

        public event SlotDelegate TileReachedToSlot;
        public event SlotDelegate TileDestroyed;
        public ITile GetTile() => currentTile;
        public void SetPosition(int x, int y)
        {
            data.x = x;
            data.y = y;
        }
        public void SetTile(ITile _tile, float duration = 0)
        {
            // Clean previous stats
            matchID = -1;
            tileMoving = false;
            data.tileData = null;
            // remove previous tile listeners
            if (currentTile != null) RemovePreviousTileListeners();
            currentTile = _tile;
            if (_tile != null)
            {
                // we have a tile
                data.tileData = _tile.data;
                _tile.OnDestroy += OnTileDestroyedByMatch;
                _tile.GotoSlot += OnTileRechedToThisSlot;
                if (duration != 0)
                {
                    tileMoving = true;
                    _tile.Reset(false);
                    _tile.SetParent(slotObject.transform, duration);
                }
                else
                {
                    _tile.Reset();
                    _tile.SetParent(slotObject.transform);
                    tileMoving = false;
                }
            }
        }

        private void OnTileRechedToThisSlot()
        {
            tileMoving = false;
            if (TileReachedToSlot != null) TileReachedToSlot.Invoke(this);
        }


        public void MarkMatch(int matchID)
        {
            this.matchID = matchID;
            if (currentTile != null) currentTile.MarkForMatch();
        }

        public void ExecuteMatch()
        {
            if (matchID != -1)
            {
                if (currentTile != null) currentTile.ExecuteMatch();
                matchID = -1;
            }
        }

        private void OnTileDestroyedByMatch(ITile tile)
        {
            SetTile(null);
            if (TileDestroyed != null) TileDestroyed.Invoke(this);
        }

        public void SetAirborn(int distance)
        {
            if (distance == 1)                                              // if airborne is 1 we do squash effect for tiles
            {
                if (currentTile != null) currentTile.Squash();
            }
            else if (distance == 0 && hasGarbage && hasTile)                // if airborne is 0 and the slot contains garbage tile, we execute the shake effect
            {
                Debug.Log("airBornDistance: " + airBornDistance);
                if (currentTile.shaked == false)
                {
                    currentTile.GarbageShake();
                }
            }
            airBornDistance = distance;
        }

        void RemovePreviousTileListeners()
        {
            currentTile.OnDestroy -= OnTileDestroyedByMatch;
            currentTile.GotoSlot -= OnTileRechedToThisSlot;
        }

        public void FillGarbage()
        {
            hasGarbage = true;
        }
        public void RemoveGarbage()
        {
            hasGarbage = false;
            matchID = -1;
        }
        public void MarkRecycle() => recyclable = true;
        public void RemoveRecyclable() => recyclable = false;
        /*
          ██████╗ ██╗   ██╗██╗
         ██╔════╝ ██║   ██║██║
         ██║  ███╗██║   ██║██║
         ██║   ██║██║   ██║██║
         ╚██████╔╝╚██████╔╝██║
          ╚═════╝  ╚═════╝ ╚═╝
        */
        private void OnGUI()
        {
            // if (data.tileData != null)
            // {
            // GUI.color = Color.black;
            // var pos = Camera.main.WorldToScreenPoint(transform.position);
            // GUI.Label(new Rect(pos.x - 20, Screen.height - pos.y, 20, 20), "D" + data.tileData.tileName);
            // GUI.Label(new Rect(pos.x - 20, Screen.height - pos.y - 20, 20, 20), "T" + currentTile.data.tileName);
            // }
            // if (hasGarbage)
            // {
            //     GUI.color = Color.black;
            //     var pos = Camera.main.WorldToScreenPoint(transform.position);
            //     GUI.Label(new Rect(pos.x, Screen.height - pos.y, 20, 20), "G");
            // }
            // if (hasTile)
            // {
            //     GUI.color = Color.black;
            //     var pos = Camera.main.WorldToScreenPoint(transform.position);
            //     GUI.Label(new Rect(pos.x - 20, Screen.height - pos.y, 20, 20), "T");
            // }
            // if (recyclable)
            // {
            //     GUI.color = Color.black;
            //     var pos = Camera.main.WorldToScreenPoint(transform.position);
            //     GUI.Label(new Rect(pos.x - 10, Screen.height - pos.y, 20, 20), "R");
            // }
            // if (airBornDistance > 0)
            // {
            // GUI.color = Color.black;
            // var pos = Camera.main.WorldToScreenPoint(transform.position);
            // GUI.Label(new Rect(pos.x, Screen.height - pos.y + 10, 20, 20), "A" + airBornDistance);
            // }
            // if (matchID != -1)
            // {
            //     GUI.color = Color.black;
            //     var pos = Camera.main.WorldToScreenPoint(transform.position);
            //     GUI.Label(new Rect(pos.x, Screen.height - pos.y + 10, 20, 20), matchID.ToString());
            // }
        }

        // TODO: freezed slots should mark a little darker and unfreezed one should be normal 
        public void Freez()
        {
            frozen = true;
            if (currentTile != null) currentTile.Freez();
        }
        public void UnFreez()
        {
            frozen = false;
            if (currentTile != null) currentTile.UnFreez();
        }
    }
}
