﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Assertions;

namespace PanelDePon
{
    public class BaseSlotManager : MonoBehaviour, ISlotManager
    {
        [SerializeField] Transform boardParent = null;
        ISlot[,] slots { get; set; }
        private IGarbageManager garbageManager;
        private GameModeManager difficultyManager;
        private TileBuilder tileBuilder;
        public int widthCount { get; private set; }
        public int heightCount { get; private set; }
        private bool boardFreez;
        private int teamIndex;
        float offsetPositionY;
        int topRowIndex;
        int gameOverRowIndex;
        float speed;
        private bool fastKeyPressed;
        private ISlot[] matchSlots;
        private float slotWidth;
        private float slotHeight;
        const int matchArrayCount = 5;
        public event VoidDelegate GameOver;
        public event ComboDelegate Combo;
        public event ComboDelegate Chain;
        // private ICheckMatch checkMatch;
        private IAirborneUpdater airborneUpdater;
        private Vector3 boardStartPosition;
        private IPlayer[] players;

        public void Initialize(int teamIndex, IPlayer[] players)
        {
            this.players = players;
            boardFreez = false;
            difficultyManager = Singleton<GameModeManager>.Instance;
            tileBuilder = Singleton<TileBuilder>.Instance;
            var boardConfig = Singleton<BoardConfigManager>.Instance.currentConfig;

            this.widthCount = boardConfig.widthCount;
            this.heightCount = boardConfig.heightCount;
            this.teamIndex = teamIndex;
            Assert.IsNotNull(boardParent);
            boardStartPosition = boardParent.transform.position;
            slots = new ISlot[widthCount, heightCount * 2];
            garbageManager = new GarbageManager();


            offsetPositionY = 0;
            topRowIndex = heightCount * 2 - 1;
            gameOverRowIndex = heightCount - 1;
            matchSlots = new ISlot[matchArrayCount];

            slotWidth = boardConfig.slotWidth;
            slotHeight = boardConfig.slotHeight;
            float startX = (widthCount * slotWidth / 2) - boardConfig.slotWidth / 2;
            float startY = (heightCount * boardConfig.slotHeight / 2) - slotHeight / 2;
            GameObject slotPrefab = boardConfig.slotPrefab.slotObject;
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount * 2; j++)
                {
                    var slotGameObject = GameObject.Instantiate(slotPrefab, new Vector2(-startX + (i * slotWidth), -startY + (j * slotHeight)), Quaternion.identity);
                    slotGameObject.name = i + "," + j;
                    slotGameObject.transform.SetParent(boardParent, false);
                    ISlot slot = slotGameObject.GetComponent<ISlot>();
                    slot.SetPosition(i, j);
                    slots[i, j] = slot;
                }
            }

            // Move player cursors to board slots
            for (int i = 0; i < players.Length; i++)
            {
                players[i].Active();
                MoveCursor((widthCount / 2) - 1, (heightCount / 2) - 1 - i, players[i], false);
            }

            // listen to player inputs
            // we wont listen here because we will listen after game resume on ready/set/go
            // AddListeners();


            // Listen to game pause and resume
            GameManager.OnResume += OnGameResumed;
            GameManager.OnPause += OnGamePaused;

            // check for game to resume speed
            CheckResumeLoop();
        }

        private void AddListeners()
        {
            foreach (var player in players)
            {
                player.Left += MoveLeft;
                player.Right += MoveRight;
                player.Up += MoveUp;
                player.Down += MoveDown;
                player.A += APressed;
                player.TriggerPressed += FastPressed;
                player.TriggerReleased += FastReleased;
                player.Start += StartPressed;
            }
        }

        void RemoveListeners()
        {
            foreach (var player in players)
            {
                player.Left -= MoveLeft;
                player.Right -= MoveRight;
                player.Up -= MoveUp;
                player.Down -= MoveDown;
                player.A -= APressed;
                player.Start -= StartPressed;
            }
        }

        private void StartPressed(int value) => GameManager.Pause();
        private void MoveLeft(int playerIndex) => MoveCursor(-1, 0, FindPlayerByIndex(playerIndex));
        private void MoveRight(int playerIndex) => MoveCursor(1, 0, FindPlayerByIndex(playerIndex));
        private void MoveUp(int playerIndex) => MoveCursor(0, 1, FindPlayerByIndex(playerIndex));
        private void MoveDown(int playerIndex) => MoveCursor(0, -1, FindPlayerByIndex(playerIndex));
        private void APressed(int playerIndex)
        {
            Debug.Log("Swap in board - pause: " + GameManager.pause);
            var player = FindPlayerByIndex(playerIndex);
            ISlot currentSlot = slots[player.cursor.X, player.cursor.Y];
            ISlot nextSlot = slots[player.cursor.X + 1, player.cursor.Y];
            // if both slots are not busy
            if (currentSlot.swapLock == false && nextSlot.swapLock == false)
            {
                if (currentSlot.hasGarbage == false && nextSlot.hasGarbage == false)
                {
                    Swap(currentSlot, nextSlot, GameModeManager.Instance.currentSetting.SwapDuration);
                    AudioManager.Instance.Play(AudioNames.swap);
                }
            }
        }
        public void FastPressed(int playerIndex)
        {
            boardFreez = CheckForBoardFreez();
            if (boardFreez == false)
            {
                SpeedUpBoard();
            }
            fastKeyPressed = true;
        }
        public void FastReleased(int playerIndex)
        {
            if (boardFreez == false)
            {
                ResumeBoard();
            }
            else
            {
                PauseBoard();
            }
            fastKeyPressed = false;
        }

        private void OnGameResumed()
        {
            Debug.Log("Repositioning Player cursors");
            foreach (var player in players)
            {
                player.Active();
                MoveCursor(0, 0, player);
            }
            AddListeners();
        }

        private void OnGamePaused()
        {
            // deactive all player cursors
            foreach (var player in players) player.Deactive();
            RemoveListeners();
        }

        private IPlayer FindPlayerByIndex(int playerIndex)
        {
            IPlayer currentPlayer = null;
            foreach (var player in players)
            {
                if (player.playerIndex == playerIndex)
                {
                    currentPlayer = player;
                    break;
                }
            }
            return currentPlayer;
        }

        public void MoveCursor(int x, int y, IPlayer player, bool playAudioEffec = true)
        {
            if (playAudioEffec) AudioManager.Instance.Play(AudioNames.moveCursor);

            x = Mathf.Clamp(player.cursor.X + x, 0, widthCount - 2);
            y = Mathf.Clamp(player.cursor.Y + y, 1, heightCount - 1);
            player.cursor.SetBoardPosition(x, y);
            player.MoveCursorToParent(slots[player.cursor.X, player.cursor.Y].slotObject.transform);
        }

        private void OnCombo(Transform highestSlot, int combo)
        {
            Debug.Log("Combo " + combo);
            if (Combo != null) Combo.Invoke(highestSlot, combo);
        }

        private void OnChain(Transform highestSlot, int combo)
        {
            Debug.Log("Chain " + combo);
            if (Chain != null) Chain.Invoke(highestSlot, combo);

            // Play chain Audio effect
            string chainAudioName = AudioNames.bigChain1;
            if (combo <= 3) chainAudioName = AudioNames.bigChain1;
            else if (combo <= 6) chainAudioName = AudioNames.bigChain2;
            else chainAudioName = AudioNames.bigChain3;
            AudioManager.Instance.Play(chainAudioName);
        }

        public void CheckMatches()
        {
            var airborneUpdater = new AirborneUpdater(slots, teamIndex);
            airborneUpdater.Chain += OnChain;
            airborneUpdater.Combo += OnCombo;
            // airborneUpdater.Complete += CheckMatchComplete;
            airborneUpdater.PauseBoard += PauseBoard;
            airborneUpdater.UpdateAirbornes();
        }

        private async void CheckResumeLoop()
        {
            var checkResumeDelayTask = Task.Delay(TimeSpan.FromSeconds(1f));
            await checkResumeDelayTask;
            boardFreez = CheckForBoardFreez();
            if (boardFreez == false || fastKeyPressed == false)
            {
                if (garbageManager.hasGarbage)
                {
                    PauseBoard();
                    DropGarbages();
                }
                else ResumeBoard();
            }
            CheckResumeLoop();
        }

        private bool CheckForBoardFreez()
        {
            // Check if there are any match or airborne or recycling slots
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    var slot = slots[i, j];
                    if (slot.matchID != -1 || slot.airBornDistance > 0 || slot.recyclable == true)
                    {
                        // Debug.Log("SlotState- matchID " + slot.matchID + " - airborneDistance: " + slot.airBornDistance + " - recyclable: " + slot.recyclable);
                        return true;
                    }
                }
            }
            return false;
        }


        /*
         ███████╗██╗    ██╗ █████╗ ██████╗ 
         ██╔════╝██║    ██║██╔══██╗██╔══██╗
         ███████╗██║ █╗ ██║███████║██████╔╝
         ╚════██║██║███╗██║██╔══██║██╔═══╝ 
         ███████║╚███╔███╔╝██║  ██║██║     
         ╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝╚═╝     
        */

        public void Swap(ISlot currentSlot, ISlot nextSlot, float duration, bool checkCompletion = true)
        {
            if (GameManager.pause == false)
            {
                // at least one of slots contain a tile
                if (currentSlot.hasTile || nextSlot.hasTile)
                {
                    // swap actual tiles
                    ITile tmpTile = nextSlot.GetTile();
                    nextSlot.SetTile(currentSlot.GetTile(), duration);
                    currentSlot.SetTile(tmpTile, duration);
                    if (checkCompletion)
                    {
                        var swappedSlot = currentSlot.hasTile ? currentSlot : nextSlot;
                        if (swappedSlot.hasTile) swappedSlot.TileReachedToSlot += SwapComplete;
                    }
                }
            }
        }

        private void SwapComplete(ISlot swappedSlot)
        {
            swappedSlot.TileReachedToSlot -= SwapComplete;
            CheckMatches();
        }

        /*
         ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗
         ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
         ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗  
         ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝  
         ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗
          ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝
        */
        public void OnUpdate()
        {
            if (GameManager.pause == false)
            {
                if (boardFreez == false)
                {
                    float offset;
                    if (fastKeyPressed == false)
                    {
                        offset = speed * (GameManager.ellapsedTime / difficultyManager.currentSetting.EllapsedTimeSpeedEffect);
                        if (offset < difficultyManager.currentSetting.NormalModeSpeed) offset = difficultyManager.currentSetting.NormalModeSpeed * Time.deltaTime;
                    }
                    else
                    {
                        // dont count game ellapsed time and just use current speed (Setting.FastModeSpeed)
                        offset = speed * Time.deltaTime;
                    }
                    offsetPositionY += offset;
                    boardParent.transform.Translate(new Vector2(0, offset));
                    if (offsetPositionY >= slotHeight)
                    {
                        offsetPositionY = 0;
                        var loss = false;
                        // Check loss if the gameOverRowIndex row slots contain any tiles
                        for (int i = 0; i < widthCount; i++)
                        {
                            if (slots[i, gameOverRowIndex].hasTile || slots[i, gameOverRowIndex].hasGarbage)
                            {
                                if (GameOver != null) GameOver();
                                loss = true;
                            }
                        }
                        // if we did not loose, recycle the top slots, fill them with tiles and move them to bottom.
                        if (!loss)
                        {
                            boardParent.transform.position = boardStartPosition;
                            // rebuild slots array to match the board visual
                            // algorithm: move row top-i to top, at the end fill row 0
                            var cacheSlots = new ISlot[widthCount];
                            for (int i = 0; i < heightCount * 2; i++)
                            {
                                for (int j = 0; j < widthCount; j++)
                                {
                                    if (i != topRowIndex)
                                    {
                                        ISlot slot = slots[j, topRowIndex - i];
                                        slot.SetTile(slots[j, topRowIndex - i - 1].GetTile());
                                        slot.UnFreez();
                                        var tile = slot.GetTile();
                                        if (tile != null && tile.data.tileType != TileTypes.GarbageTileType) slot.RemoveGarbage();
                                    }
                                }
                            }
                            FillRow(0);
                            // now freez first row
                            for (int i = 0; i < widthCount; i++)
                            {
                                slots[i, 0].Freez();
                            }

                            // Update player cursors after board update
                            foreach (var player in players)
                            {
                                if (player.cursor.Y == heightCount - 1) MoveCursor(0, 0, player, false);
                                else MoveCursor(0, 1, player, false);
                            }

                            CheckMatches();
                        }
                    }
                }
            }
        }

        private void DropGarbages()
        {
            var garbages = garbageManager.DropGarbage();
            for (int i = 0; i < garbages.Length; i++)
            {
                ITile garbage = garbages[i];
                // find highest column to drop tiles there
                var highestColumnIndex = 0;
                var maxHeight = 0;
                for (int j = 0; j < widthCount; j++)
                {
                    int columnHeight = 0;
                    for (int k = 0; k < heightCount * 2; k++)
                    {
                        if (slots[j, k].hasTile) columnHeight++;
                    }
                    if (maxHeight < columnHeight)
                    {
                        maxHeight = columnHeight;
                        highestColumnIndex = j;
                    }
                }
                Debug.Log("Dropping garbage: " + garbage.data.tileName + " at highest column: " + highestColumnIndex);
                // find correct garbage column depend on the highest column position and the garbage width
                int garbageColumnIndex = 0;
                if (highestColumnIndex >= widthCount / 2) garbageColumnIndex = widthCount - garbage.data.width;

                int dropppingHeight = maxHeight >= heightCount ? maxHeight + 1 + i : heightCount + i;

                ISlot slot = slots[garbageColumnIndex, dropppingHeight];
                slot.SetTile(garbage);
            }
            CheckMatches();
        }

        void PauseBoard()
        {
            boardFreez = true;
            speed = 0f;
        }
        void ResumeBoard()
        {
            boardFreez = false;
            if (fastKeyPressed == false)
                speed = difficultyManager.currentSetting.NormalModeSpeed;
        }
        void SpeedUpBoard() => speed = difficultyManager.currentSetting.FastModeSpeed;

        /*
         ██████╗ ██████╗  ██████╗ ██████╗      ██████╗  █████╗ ██████╗ ██████╗  █████╗  ██████╗ ███████╗███████╗
         ██╔══██╗██╔══██╗██╔═══██╗██╔══██╗    ██╔════╝ ██╔══██╗██╔══██╗██╔══██╗██╔══██╗██╔════╝ ██╔════╝██╔════╝
         ██║  ██║██████╔╝██║   ██║██████╔╝    ██║  ███╗███████║██████╔╝██████╔╝███████║██║  ███╗█████╗  ███████╗
         ██║  ██║██╔══██╗██║   ██║██╔═══╝     ██║   ██║██╔══██║██╔══██╗██╔══██╗██╔══██║██║   ██║██╔══╝  ╚════██║
         ██████╔╝██║  ██║╚██████╔╝██║         ╚██████╔╝██║  ██║██║  ██║██████╔╝██║  ██║╚██████╔╝███████╗███████║
         ╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚═╝          ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝
        */
        public void ReceiveGarbageAttack(ITile[] garbageTile)
        {
            garbageManager.AddGarbage(garbageTile);
        }

        public void FillRow(int rowIndex)
        {
            ITile[] tiles = tileBuilder.GetRandomNormalTilesInRow(widthCount);
            for (int i = 0; i < widthCount; i++)
            {
                slots[i, rowIndex].SetTile(tiles[i]);
                if (rowIndex == 0) slots[i, rowIndex].Freez();
            }
        }

        public void Fill(int x, int y, ITile tile)
        {
            slots[x, y].SetTile(tile);
        }

        void OnDisable()
        {
            Debug.Log("Dispose Base slot manager");
            RemoveListeners();

            foreach (var player in players) player.Deactive();
            // Remove Listen to game pause and resume
            GameManager.OnResume -= OnGameResumed;
            GameManager.OnPause -= OnGamePaused;
        }
    }
}
