namespace PanelDePon
{
    public static class TileTypes
    {
        public static readonly string GarbageTileType = "garbage";
        public static readonly string NormalTileType = "normal";
    }
}