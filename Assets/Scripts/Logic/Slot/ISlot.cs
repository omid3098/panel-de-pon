using UnityEngine;
namespace PanelDePon
{
    public interface ISlot
    {
        // position of the slot in the board. first value is width and the second one is height
        // int[] position { get; }
        void SetPosition(int x, int y);
        SlotData data { get; }
        bool swapLock { get; }
        bool frozen { get; }
        GameObject slotObject { get; }
        bool hasTile { get; }
        bool hasGarbage { get; }
        bool recyclable { get; }
        int matchID { get; }
        string tileName { get; }
        int airBornDistance { get; }
        event SlotDelegate TileReachedToSlot;
        event SlotDelegate TileDestroyed;
        void ExecuteMatch();
        ITile GetTile();
        void SetTile(ITile tile, float duration = 0);
        void MarkMatch(int matchID);
        void SetAirborn(int distance);
        void FillGarbage();
        void RemoveGarbage();
        void MarkRecycle();
        void RemoveRecyclable();
        void Freez();
        void UnFreez();
    }
}