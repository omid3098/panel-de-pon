using System.Collections.Generic;

namespace PanelDePon
{
    public class GarbageManager : IGarbageManager
    {
        public bool hasGarbage => garbageList.Count != 0;
        private List<ITile> garbageList = new List<ITile>();
        public void AddGarbage(ITile[] garbageTile)
        {
            foreach (var garbage in garbageTile)
            {
                if (garbage != null)
                    garbageList.Add(garbage);
            }
        }

        public ITile[] DropGarbage()
        {
            ITile[] result = new ITile[garbageList.Count];
            for (int i = 0; i < garbageList.Count; i++)
            {
                result[i] = garbageList[i];
            }
            garbageList.Clear();
            return result;
        }
    }

    public interface IGarbageManager
    {
        bool hasGarbage { get; }

        void AddGarbage(ITile[] garbageTile);
        ITile[] DropGarbage();
    }
}