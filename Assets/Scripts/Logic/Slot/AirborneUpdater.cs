using UnityEngine;
namespace PanelDePon
{
    public class AirborneUpdater : IAirborneUpdater
    {
        ISlot[,] slots;
        private int widthCount;
        private int heightCount;
        private ICheckMatch checkMatch;
        private int highestChainValue;
        private Transform highestSlot;
        private int currentAirborneTilesCounter;

        public event VoidDelegate Complete;
        public event ComboDelegate Combo;
        public event ComboDelegate Chain;
        public event VoidDelegate PauseBoard;
        public AirborneUpdater(ISlot[,] slots, int teamIndex)
        {
            this.slots = slots;
            this.widthCount = slots.GetLength(0);
            this.heightCount = slots.GetLength(1);
            checkMatch = new CheckMatch(slots, teamIndex);
            checkMatch.Combo += OnCombo;
            checkMatch.Chain += OnChain;
            checkMatch.MatchFound += MatchFound;
            checkMatch.Complete += CheckMatchComplete;
        }

        // /*
        //  ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗     █████╗ ██╗██████╗ ██████╗  ██████╗ ██████╗ ███╗   ██╗███████╗
        //  ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝    ██╔══██╗██║██╔══██╗██╔══██╗██╔═══██╗██╔══██╗████╗  ██║██╔════╝
        //  ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗      ███████║██║██████╔╝██████╔╝██║   ██║██████╔╝██╔██╗ ██║███████╗
        //  ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝      ██╔══██║██║██╔══██╗██╔══██╗██║   ██║██╔══██╗██║╚██╗██║╚════██║
        //  ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗    ██║  ██║██║██║  ██║██████╔╝╚██████╔╝██║  ██║██║ ╚████║███████║
        //   ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝    ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝                                                                                                                   
        //  */

        public void UpdateAirbornes()
        {
            ResetGarbageStats();
            currentAirborneTilesCounter = 0;
            for (int i = 0; i < widthCount; i++)
            {
                var distance = 0;
                for (int j = 0; j < heightCount; j++)
                {
                    // if we have an empty slot, increase distance
                    // and if we have tile or the slot is filled with garbage, set airborn for them
                    // we use garbage airbornes for find the lowest airborne for the whole pack
                    ISlot slot = slots[i, j];
                    bool hastile = slot.hasTile;
                    bool hasGarbage = slot.hasGarbage;
                    bool recyclable = slot.recyclable;
                    int matchID = slot.matchID;
                    if (hastile == false)
                    {
                        if (hasGarbage == false)
                        {
                            if (recyclable == false)
                            {
                                // Empty tiles not recycling:
                                // hasTile == false
                                // hasGarbage == false
                                // recyclable == false
                                // => Set(distance) => distance ++
                                slot.SetAirborn(0);
                                distance++;
                            }
                            else // recyclable
                            {
                                // Empty tiles while recycling:
                                // hasTile == false
                                // hasGarbage == false
                                // recyclable == true
                                // distance = 0 => Set(distance)
                                distance = 0;
                                slot.SetAirborn(distance);
                            }
                        }
                        else // has garbage
                        {
                            if (recyclable == false)
                            {
                                if (matchID == -1)
                                {
                                    // Empty tiles Marked as garbage:
                                    // hastile == false
                                    // hasGarbage == true
                                    // markedMatch == false
                                    // recyclable == false
                                    // => Set(distance) => distance = 0
                                    slot.SetAirborn(distance);
                                    distance = 0;
                                }
                                else // marked for match == true
                                {
                                    // Empty tiles Marked as garbage not recycling but marked for match:
                                    // hastile == false
                                    // hasGarbage == true
                                    // markedMatch == true
                                    // recyclable == false
                                    // => Set(distance) => distance = 0
                                    slot.SetAirborn(distance);
                                    distance = 0;
                                }
                            }
                            else // recyclable == true
                            {
                                // Empty tiles Marked as garbage recycling
                                // hastile == false
                                // hasGarbage == true
                                // markedMatch == false
                                // recyclable == true
                                // => Set(distance) => distance = 0
                                distance = 0;
                                slot.SetAirborn(distance);
                            }

                        }
                    }
                    else // hasTile == true
                    {
                        // falling Tiles: same as Normal tiles
                        if (slot.GetTile().data.tileType == TileTypes.NormalTileType)
                        {
                            if (matchID == -1)
                            {
                                if (recyclable == false)
                                {
                                    // Normal tiles:
                                    // hastile == true
                                    // tileType == normal
                                    // markedMatch == false
                                    // recyclable == false
                                    // => Set(distance)
                                    slot.SetAirborn(distance);
                                }
                                else
                                {
                                    // Recycled normal tiles:
                                    // hastile == true
                                    // tileType == normal
                                    // markedMatch == false
                                    // recyclable == true
                                    // => Set(distance)
                                    distance = 0;
                                    slot.SetAirborn(distance);
                                }
                            }
                            else // markedForMatch == true
                            {
                                // Normal tiles while matching:
                                // hastile == true
                                // tileType == normal
                                // markedMatch == true
                                // => Set(distance)
                                distance = 0;
                                slot.SetAirborn(distance);
                            }
                        }
                        else // garbage tiles
                        {
                            if (matchID == -1)
                            {
                                // Garbage tiles
                                // hasTile == true
                                // hasGarbage == true
                                // markMatched == false
                                // recyclable == false || recyclable == true
                                slot.SetAirborn(distance);
                                distance = 0;
                            }
                            else
                            {
                                // Garbage tiles marked for match
                                // hasTile == true
                                // hasGarbage == true
                                // markMatched == true
                                // recyclable == false || recyclable == true
                                distance = 0;
                                slot.SetAirborn(distance);
                            }
                        }
                    }
                }
            }

            // Update airbornes for slots with garbage tiles not those marked as filled with garbage
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    ISlot slot = slots[i, j];
                    if (slot.hasTile && slot.hasGarbage)
                    {
                        // slots with actual garbage tile
                        // find the lowest airborne distance in slots garbage width
                        var garbageTile = slot.GetTile();
                        int minAirborne = heightCount * 2;
                        for (int k = 0; k < garbageTile.data.width; k++)
                        {
                            int airBornDistance = slots[i + k, j].airBornDistance;
                            // Debug.Log("Garbage airborne at " + k + " : " + airBornDistance);
                            if (airBornDistance < minAirborne) minAirborne = airBornDistance;
                        }
                        // Debug.Log("Min airborne:" + minAirborne);
                        slot.SetAirborn(minAirborne);
                    }
                    // set airborne for slots without tiles to 0
                    if (slot.hasTile == false && slot.hasGarbage)
                    {
                        slot.SetAirborn(0);
                    }
                    // if we have any airborned slots, count it:
                    if (slot.airBornDistance > 0)
                    {
                        currentAirborneTilesCounter++;
                        // Debug.Log("airborne: " + slot.airBornDistance + " - at: " + slot.data.x + "," + slot.data.y);
                    }
                }
            }
            // Debug.Log(currentAirborneTilesCounter + " slots have airborne");

            if (currentAirborneTilesCounter > 0)
            {
                MoveAirbornsDown(currentAirborneTilesCounter);
            }
            else
            {
                // CheckMatches();
                // Debug.Log("Airborne update complete, Checking matches");
                checkMatch.Execute();
            }
        }

        private void MatchFound()
        {
            // Debug.Log("Match Found!");
            AudioManager.Instance.Play(AudioNames.chain);
            if (PauseBoard != null) PauseBoard.Invoke();
        }

        private void CheckMatchComplete(bool matchFound)
        {
            if (matchFound)
            {
                // Debug.Log("Re-Updating airbornes");
                UpdateAirbornes();
            }
            else
            {
                // Debug.Log("Check match complete!");
                if (highestSlot != null) if (Chain != null) Chain.Invoke(highestSlot, highestChainValue);
                if (Complete != null) Complete.Invoke();
            }
        }

        private void OnCombo(Transform highestSlot, int value)
        {
            if (Combo != null) Combo.Invoke(highestSlot, value);
        }

        private void OnChain(Transform highestSlot, int value)
        {
            this.highestChainValue = value;
            this.highestSlot = highestSlot;
        }

        private void ResetGarbageStats()
        {
            // clean previous garbage stats
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    ISlot slot = slots[i, j];
                    if (slot.hasTile == false && slot.hasGarbage)
                    {
                        slot.RemoveGarbage();
                        // Debug.Log("Removing garbage from empty slots: " + i + "," + j);
                    }
                }
            }
            // refill slots with garbage
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    ISlot slot = slots[i, j];
                    ITile tile = slot.GetTile();
                    if (tile != null && tile.data.tileType == TileTypes.GarbageTileType)
                    {
                        // re update garbage slots
                        for (int k = 0; k < tile.data.width; k++)
                        {
                            for (int l = 0; l < tile.data.height; l++)
                            {
                                slots[i + k, j + l].FillGarbage();
                                // Debug.Log("Filling slot with garbage");
                            }
                        }
                    }
                }
            }
        }

        public void MoveAirbornsDown(int airborneTilesCounter)
        {
            int counter = 0;
            var setting = Singleton<GameModeManager>.Instance.currentSetting;
            for (int i = 0; i < widthCount; i++)
            {
                for (int j = 0; j < heightCount; j++)
                {
                    ISlot slot = slots[i, j];
                    if (slot.airBornDistance > 0)
                    {
                        counter++;
                        if (counter != airborneTilesCounter) FallDown(slot, slots[i, j - 1], setting.FallDownDurationPerTile, false);
                        else FallDown(slot, slots[i, j - 1], setting.FallDownDurationPerTile);
                    }
                }
            }
        }


        public void FallDown(ISlot currentSlot, ISlot nextSlot, float duration, bool checkCompletion = true)
        {
            // at least one of slots contain a tile
            if (currentSlot.hasTile || nextSlot.hasTile)
            {
                // swap actual tiles
                ITile tmpTile = nextSlot.GetTile();
                nextSlot.SetTile(currentSlot.GetTile(), duration);
                currentSlot.SetTile(tmpTile, duration);
                if (checkCompletion)
                {
                    var swappedSlot = currentSlot.hasTile ? currentSlot : nextSlot;
                    if (swappedSlot.hasTile) swappedSlot.TileReachedToSlot += FallComplete;
                }
            }
        }

        private void FallComplete(ISlot swappedSlot)
        {
            swappedSlot.TileReachedToSlot -= FallComplete;
            UpdateAirbornes();
        }
    }

    public interface IAirborneUpdater
    {
        event VoidDelegate Complete;
        event VoidDelegate PauseBoard;
        event ComboDelegate Combo;
        event ComboDelegate Chain;
        void UpdateAirbornes();
    }
}
