/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using UnityEngine;

namespace PanelDePon
{
    public class BoardConfigManager : MonoBehaviour
    {
        [SerializeField] BoardConfig config_6_12 = null;
        [SerializeField] BoardConfig config_7_12 = null;
        [SerializeField] BoardConfig config_8_12 = null;

        public static BoardConfigManager Instance { get; private set; }
        public BoardConfig currentConfig { get; private set; }
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
            currentConfig = config_7_12;
            Debug.Log("current config" + currentConfig.name);
        }
        public void SetWidth(int width)
        {
            if (width == 6) currentConfig = config_6_12;
            else if (width == 7) currentConfig = config_7_12;
            else if (width == 8) currentConfig = config_8_12;
        }
    }
}