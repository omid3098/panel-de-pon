using UnityEngine;

namespace PanelDePon
{
    public class BasicTile : ITile
    {
        public BasicTile(ITileView tileView)
        {
            tileView.tileGameObject.SetActive(false);
            this.tileView = tileView;
            data = new TileData(tileView.tileName, tileView.tileType, tileView.width, tileView.height);
            tileView.Destroyed += OnViewDestroyed;
            tileView.MoveComplete += OnMoveComplete;
        }
        ~BasicTile()
        {
            tileView.Destroyed -= OnViewDestroyed;
            tileView.MoveComplete -= OnMoveComplete;
        }

        public event VoidDelegate GotoSlot;
        public event TileDelegate OnDestroy;
        protected ITileView tileView { get; set; }
        public int width => tileView.width;
        public int height => tileView.height;
        public TileData data { get; private set; }

        public bool shaked { get; private set; }

        public void ExecuteMatch()
        {
            tileView.RunDestory();
        }

        private void OnViewDestroyed()
        {
            if (OnDestroy != null) OnDestroy.Invoke(this);
        }

        public void Squash() => tileView.Squash();
        public void MarkForMatch()
        {
            tileView.MatchedEffect();
        }

        public void Reset(bool resetTile = true)
        {
            if (resetTile) tileView.Reset();
        }

        public void SetParent(Transform transform, float duration = 0)
        {
            tileView.tileGameObject.transform.SetParent(transform);
            tileView.LocalMove(Vector3.zero, duration);
        }

        private void OnMoveComplete()
        {
            if (GotoSlot != null) GotoSlot.Invoke();
        }

        public void SetParent(Transform transform)
        {
            tileView.tileGameObject.transform.SetParent(transform, false);
        }

        public void Freez()
        {
            tileView.Freez();
        }

        public void UnFreez()
        {
            tileView.UnFreez();
        }

        public void GarbageShake()
        {
            shaked = true;
            tileView.GarbageShake();
        }
    }
}