using System;
using System.Collections;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
namespace PanelDePon
{
    public class TileView : MonoBehaviour, ITileView
    {
        private const float animationFrameWait = 0.1f;
        public string tileName => _tileName;
        public GameObject tileGameObject => gameObject;
        public string tileType => TileTypes.NormalTileType;
        public int width { get { return 1; } set { } }
        public int height { get { return 1; } set { } }
        [SerializeField] string _tileName = "";
        Sprite normalSprite;
        [SerializeField] Sprite[] fallingAnimation = null;
        [SerializeField] Sprite matchedSprite = null;
        [SerializeField] Sprite destroySprite = null;
        [SerializeField] GameObject destroyParticleEffect = null;
        private SpriteRenderer spriteRenderer;
        private Color startColor;
        private Vector3 startScale;
        public event VoidDelegate Destroyed;
        public event VoidDelegate MoveComplete;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            normalSprite = spriteRenderer.sprite;
            startColor = spriteRenderer.color;
            startScale = transform.localScale;
        }
        public void Reset()
        {
            gameObject.SetActive(true);
            transform.localScale = startScale;
            spriteRenderer.color = startColor;
            transform.localPosition = Vector3.zero;
            spriteRenderer.sprite = normalSprite;
        }

        public void RunDestory()
        {
            AudioManager.Instance.Play(AudioNames.tilePop);
            var tween = transform.DOScale(Vector3.zero, 0.1f);
            var particleGo = Instantiate(destroyParticleEffect);
            particleGo.transform.position = transform.position;
            tween.onComplete += () =>
            {
                if (Destroyed != null) Destroyed.Invoke();
                gameObject.SetActive(false);
            };
        }
        public void MatchedEffect()
        {
            StartCoroutine(MatchEffectCoroutine());
        }

        private IEnumerator MatchEffectCoroutine()
        {
            spriteRenderer.sprite = matchedSprite;
            yield return new WaitForSeconds(animationFrameWait);
            spriteRenderer.sprite = normalSprite;
            yield return new WaitForSeconds(animationFrameWait * 2);
            spriteRenderer.sprite = matchedSprite;
            yield return new WaitForSeconds(animationFrameWait * 2);
            spriteRenderer.sprite = normalSprite;
            yield return new WaitForSeconds(animationFrameWait);
            spriteRenderer.sprite = destroySprite;
        }


        public void LocalMove(Vector3 zero, float duration)
        {
            var tween = transform.DOLocalMove(Vector3.zero, duration);
            tween.onComplete += () =>
            {
                if (MoveComplete != null) MoveComplete.Invoke();
            };
        }

        public void Squash()
        {
            if (gameObject.activeInHierarchy) StartCoroutine(FallAnimation());
            // Scale down to 0.5 and then scale up to 1 in 0.4 second
            // transform.localScale = new Vector3(startScale.x, startScale.y * 0.8f, startScale.z);
            // transform.DOScaleY(startScale.y, 0.4f);
        }

        private IEnumerator FallAnimation()
        {
            spriteRenderer.sprite = fallingAnimation[0];
            yield return new WaitForSeconds(animationFrameWait);
            spriteRenderer.sprite = fallingAnimation[1];
            yield return new WaitForSeconds(animationFrameWait);
            spriteRenderer.sprite = fallingAnimation[2];
            yield return new WaitForSeconds(animationFrameWait);
            spriteRenderer.sprite = normalSprite;
        }

        public void Freez()
        {
            spriteRenderer.color = Color.gray;
        }

        public void UnFreez()
        {
            spriteRenderer.color = Color.white;
        }

        public void GarbageShake() { }
        // private void OnGUI()
        // {
        //     var screenPos = Camera.main.WorldToScreenPoint(transform.position);
        //     GUI.Label(new Rect(screenPos.x, Screen.height - screenPos.y, 100, 20), tileType);
        // }
    }
}