using UnityEngine;
namespace PanelDePon
{
    public interface ITileView
    {
        string tileName { get; }
        string tileType { get; }
        GameObject tileGameObject { get; }
        int width { get; set; }
        int height { get; set; }

        event VoidDelegate Destroyed;
        event VoidDelegate MoveComplete;
        void RunDestory();
        void Squash();
        void MatchedEffect();
        void Reset();
        void LocalMove(Vector3 zero, float duration);
        void Freez();
        void UnFreez();
        void GarbageShake();
    }
}