﻿using System;
using UnityEngine;
namespace PanelDePon
{
    public enum AttackType
    {
        combo,
        chain,
    }
    public class TileBuilder : Singleton<TileBuilder>
    {
        private ITile[] cachedRow;
        private GameModeManager gameModeManager;
        private TileTheme theme;
        private BoardConfigManager boardConfigManager;

        public void Initialize()
        {
            if (cachedRow != null) Array.Clear(cachedRow, 0, cachedRow.Length);
            cachedRow = null;
            gameModeManager = Singleton<GameModeManager>.Instance;
            boardConfigManager = Singleton<BoardConfigManager>.Instance;
            gameModeManager.SettingChanged += SettingChanged;
            theme = gameModeManager.currentSetting.theme;
        }

        private void OnDisable()
        {
            gameModeManager.SettingChanged -= SettingChanged;
        }

        private void SettingChanged()
        {
            theme = gameModeManager.currentSetting.theme;
        }

        public ITile[] GetRandomNormalTilesInRow(int count)
        {
            if (cachedRow == null)
            {
                cachedRow = MakeNewRow(count);
                return cachedRow;
            }
            else
            {
                ITile[] tiles = new ITile[count];
                for (int i = 0; i < count; i++)
                {
                    var tile = GetRandomNormalTile();
                    if (i > 0)
                    {
                        // compare with cached row and previous tile
                        while (tile.data.tileName == cachedRow[i].data.tileName || tile.data.tileName == tiles[i - 1].data.tileName)
                        {
                            tile = GetRandomNormalTile();
                        }
                    }
                    else
                    {
                        // only compare with cached row
                        while (tile.data.tileName == cachedRow[i].data.tileName)
                        {
                            tile = GetRandomNormalTile();
                        }
                    }
                    tiles[i] = tile;
                }
                cachedRow = tiles;
                return tiles;
            }
        }

        private ITile[] MakeNewRow(int count)
        {
            // make a new row with non similar neighbour tiles
            ITile[] tiles = new ITile[count];
            for (int i = 0; i < count; i++)
            {
                ITile tile = GetRandomNormalTile();
                if (i != 0)
                {
                    while (tile.data.tileName == tiles[i - 1].data.tileName)
                    {
                        tile = GetRandomNormalTile();
                    }
                }
                tiles[i] = tile;
            }
            return tiles;
        }

        public ITile[] GetGarbage(int strenght, AttackType attackType, int team)
        {
            ITile[] garbages = null;
            // Handle different combos
            if (attackType == AttackType.combo)
            {
                if (strenght < 8)
                {
                    garbages = new ITile[1];
                    garbages[0] = GetLightGarbage(strenght - 1 + boardConfigManager.currentConfig.widthCount - 6, team);
                }
                else if (strenght == 8)
                {
                    garbages = new ITile[2];
                    garbages[0] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 3, team);
                    garbages[1] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 2, team);
                }
                else if (strenght == 9)
                {
                    garbages = new ITile[2];
                    garbages[0] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 2, team);
                    garbages[1] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 1, team);
                }
                else if (strenght == 10)
                {
                    garbages = new ITile[2];
                    garbages[0] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 1, team);
                    garbages[1] = GetLightGarbage(boardConfigManager.currentConfig.widthCount - 1, team);
                }
                else if (strenght == 11)
                {
                    garbages = new ITile[2];
                    garbages[0] = GetLightGarbage(boardConfigManager.currentConfig.widthCount, team);
                    garbages[1] = GetLightGarbage(boardConfigManager.currentConfig.widthCount, team);
                }
                else if (strenght == 12)
                {
                    garbages = new ITile[3];
                    garbages[0] = GetLightGarbage(boardConfigManager.currentConfig.widthCount, team);
                    garbages[1] = GetLightGarbage(boardConfigManager.currentConfig.widthCount, team);
                    garbages[2] = GetLightGarbage(boardConfigManager.currentConfig.widthCount, team);
                }
                return garbages;
            }
            else
            {
                // Handle Chain garbages: now the strenght is chain amount
                garbages = new ITile[1];
                ITileView garbageTileView = GenerateGarbage(boardConfigManager.currentConfig.widthCount, strenght, team);
                garbages[0] = new GarbageTile(garbageTileView);
                return garbages;
            }
        }

        ITile GetLightGarbage(int strenght, int team)
        {
            ITileView garbageTileView = GenerateGarbage(strenght, 1, team);
            var garbageTile = new GarbageTile(garbageTileView);
            return garbageTile;
        }

        private ITileView GenerateGarbage(int width, int height, int team)
        {
            var garbageTileView = new GameObject().AddComponent<GarbageTileView>();
            garbageTileView.width = width;
            garbageTileView.height = height;
            garbageTileView.tileGameObject.name = garbageTileView.tileName;
            var garbageSpritePack = team == 1 ? theme.team2Garbages : theme.team1Garbages;
            SingleGarbageTileView[,] garbageArray = new SingleGarbageTileView[width, height];
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    // var child = GameObject.Instantiate(tilePrefab);
                    SingleGarbageTileView singleGarbageTileView = GameObject.Instantiate(theme.tilePrefab);
                    singleGarbageTileView.transform.SetParent(garbageTileView.transform, false);
                    singleGarbageTileView.SetOrder(height);
                    singleGarbageTileView.transform.position = new Vector3(i, j, 0);
                    if (height == 1)
                    {
                        if (i == 0) singleGarbageTileView.SetSprite(garbageSpritePack.oneRowLeft);
                        else if (i == width - 1) singleGarbageTileView.SetSprite(garbageSpritePack.oneRowRight);
                        else singleGarbageTileView.SetSprite(garbageSpritePack.oneRowMiddle);
                    }
                    else
                    {
                        if (i == 0 && j == 0) singleGarbageTileView.SetSprite(garbageSpritePack.lowerLeft);
                        else if (i == width - 1 && j == 0) { singleGarbageTileView.SetSprite(garbageSpritePack.lowerLeft); singleGarbageTileView.FlipX(true); }
                        else if (j == 0) singleGarbageTileView.SetSprite(garbageSpritePack.lowerMiddle);
                        else if (i == 0 && j == height - 1) singleGarbageTileView.SetSprite(garbageSpritePack.topLeft);
                        else if (i == width - 1 && j == height - 1) { singleGarbageTileView.SetSprite(garbageSpritePack.topLeft); singleGarbageTileView.FlipX(true); }
                        else if (j == height - 1) singleGarbageTileView.SetSprite(garbageSpritePack.topMiddle);
                        else if (i == 0) singleGarbageTileView.SetSprite(garbageSpritePack.centerLeft);
                        else if (i == width - 1) { singleGarbageTileView.SetSprite(garbageSpritePack.centerLeft); singleGarbageTileView.FlipX(true); }
                        else singleGarbageTileView.SetSprite(garbageSpritePack.centerMiddle);
                    }
                    garbageArray[i, j] = singleGarbageTileView;
                }
            }
            garbageTileView.SetupGarbages(garbageArray);
            return garbageTileView;
        }

        //use this method to load saved boards and generate tiles by their data
        public ITile GetTile(TileData tileData, int teamIndex)
        {
            ITileView tilePrefab = null;
            if (tileData.tileType == TileTypes.NormalTileType)
            {
                for (int i = 0; i < theme.normalTiles.Length; i++)
                {
                    var prefab = theme.normalTiles[i];
                    if (prefab.tileType == tileData.tileType && prefab.tileName == tileData.tileName)
                    {
                        tilePrefab = prefab;
                        break;
                    }
                }
                var tileView = GameObject.Instantiate(tilePrefab.tileGameObject).GetComponent<ITileView>();
                var tile = new BasicTile(tileView);
                return tile;
            }
            else if (tileData.tileType == TileTypes.GarbageTileType)
            {
                tilePrefab = GenerateGarbage(tileData.width, tileData.height, teamIndex);
                var garbageTile = new GarbageTile(tilePrefab);
                return garbageTile;
            }
            return null;
        }

        public ITile GetRandomNormalTile()
        {
            ITileView tilePrefab = theme.normalTiles[UnityEngine.Random.Range(0, theme.normalTiles.Length)];
            var tileView = GameObject.Instantiate(tilePrefab.tileGameObject).GetComponent<ITileView>();
            var tile = new BasicTile(tileView);
            return tile;
        }
    }
}