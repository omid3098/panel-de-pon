using MessagePack;

namespace PanelDePon
{
    [MessagePackObject]
    public class TileData
    {
        public TileData(string tileName, string tileType, int width, int height)
        {
            this.tileName = tileName;
            this.tileType = tileType;
            this.width = width;
            this.height = height;
        }

        [Key(0)] public string tileName { get; set; }
        [Key(1)] public string tileType { get; set; }
        [Key(2)] public int width { get; set; }
        [Key(3)] public int height { get; set; }
    }
}