using System;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Assertions;

namespace PanelDePon
{
    public class GarbageTileView : MonoBehaviour, ITileView
    {
        public int width { get; set; }
        public int height { get; set; }
        private Vector3 startScale;
        private IBoardBehaviour parentBoard;

        public string tileName => "garbage-" + width + "-" + height;
        public string tileType => TileTypes.GarbageTileType;
        public GameObject tileGameObject => gameObject;
        SingleGarbageTileView[,] _singleGarbageTileViews;
        public void SetupGarbages(SingleGarbageTileView[,] singleGarbageTileViews)
        {
            this._singleGarbageTileViews = singleGarbageTileViews;
        }
        public event VoidDelegate Destroyed;
        public event VoidDelegate MoveComplete;
        private void Awake()
        {
            startScale = transform.localScale;
        }
        public void LocalMove(Vector3 zero, float duration)
        {
            var tween = transform.DOLocalMove(Vector3.zero, duration);
            tween.onComplete += () =>
            {
                if (MoveComplete != null) MoveComplete.Invoke();
            };
        }

        public void MatchedEffect()
        {
            foreach (var singleTile in _singleGarbageTileViews) singleTile.MatchEffect();
        }

        public void Reset()
        {
            gameObject.SetActive(true);
            transform.localScale = startScale;
            transform.localPosition = Vector3.zero;
        }

        public async void RunDestory()
        {
            const float Duration = 0.1f;
            int childCount = transform.childCount;
            var width = _singleGarbageTileViews.GetLength(0);
            var height = _singleGarbageTileViews.GetLength(1);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    var single = _singleGarbageTileViews[j, i];
                    AudioManager.Instance.Play(AudioNames.tilePop);
                    var tween = single.transform.DOScale(Vector3.zero, Duration);
                    Task task = Task.Delay(TimeSpan.FromSeconds(Duration));
                    await task;
                }
            }
            if (Destroyed != null) Destroyed.Invoke();
            gameObject.SetActive(false);


            // // scale down each child after delay 
            // for (int i = 0; i < childCount; i++)
            // {
            //     var child = transform.GetChild(i);
            //     var tween = child.DOScale(Vector3.zero, Duration);
            //     AudioManager.Play(AudioNames.tilePop);
            //     Task task = Task.Delay(TimeSpan.FromSeconds(Duration));
            //     await task;
            //     // fir the last child, when tween completed, invoke destroy event for this tile
            //     if (i == childCount - 1)
            //     {
            //         tween.onComplete += () =>
            //         {
            //             if (Destroyed != null) Destroyed.Invoke();
            //             gameObject.SetActive(false);
            //         };
            //     }
            // }
        }


        public void Freez()
        {
        }

        public void UnFreez()
        {
        }

        public void Squash()
        {
            // Debug.LogError("Implement Squash effect for garbages");
        }

        public void GarbageShake()
        {
            if (parentBoard == null) parentBoard = GetComponentInParent<IBoardBehaviour>();
            Assert.IsNotNull(parentBoard);
            parentBoard.Shake();
        }
    }
}