using System;
using UnityEngine;
namespace PanelDePon
{
    public class SingleGarbageTileView : MonoBehaviour
    {
        [SerializeField] SpriteRenderer _spriteRenderer = null;
        [SerializeField] Sprite unlock_sprite = null;
        [SerializeField] Sprite unlock_sprite_white = null;
        private Sprite baseSprite;

        private void Awake()
        {
            baseSprite = _spriteRenderer.sprite;
        }
        public void SetSprite(Sprite sprite)
        {
            _spriteRenderer.sprite = sprite;
        }

        internal void FlipX(bool value)
        {
            _spriteRenderer.flipX = value;
        }

        internal void MatchEffect()
        {
            _spriteRenderer.sprite = unlock_sprite;
        }
        public void ResetSprite()
        {
            _spriteRenderer.sprite = baseSprite;
        }

        internal void SetOrder(int height)
        {
            _spriteRenderer.sortingOrder = height;
        }
    }
}