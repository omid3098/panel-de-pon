/*
 * File Created: Tuesday, 20th August 2019 6:05:09 pm
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Tuesday, 20th August 2019 6:05:11 pm
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

namespace PanelDePon
{
    public class GarbageTile : BasicTile
    {
        public GarbageTile(ITileView tileView) : base(tileView) { }
    }
}