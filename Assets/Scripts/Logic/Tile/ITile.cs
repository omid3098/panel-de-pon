using UnityEngine;

namespace PanelDePon
{
    public interface ITile
    {
        TileData data { get; }
        bool shaked { get; }

        event TileDelegate OnDestroy;
        event VoidDelegate GotoSlot;
        void ExecuteMatch();
        void MarkForMatch();
        void Reset(bool resetTile = true);
        void SetParent(Transform transform, float duration);
        void SetParent(Transform transform);
        void Squash();
        void Freez();
        void UnFreez();
        void GarbageShake();
    }
}