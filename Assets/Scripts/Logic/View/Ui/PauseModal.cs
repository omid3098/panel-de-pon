namespace PanelDePon
{
    public class PauseModal : BaseMenuManager
    {
        internal void AddListeners()
        {
            GameManager.OnPause += OnPause;
            GameManager.OnResume += OnResume;
        }

        public void RemoveListeners()
        {
            GameManager.OnPause -= OnPause;
            GameManager.OnResume -= OnResume;
        }

        private void OnPause() { gameObject.SetActive(true); }
        private void OnResume() { gameObject.SetActive(false); }
    }
}