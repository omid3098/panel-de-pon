namespace PanelDePon
{
    public class ResumeMenuItem : MenuItem
    {
        public override void Select()
        {
            GameManager.Resume();
        }
    }
}