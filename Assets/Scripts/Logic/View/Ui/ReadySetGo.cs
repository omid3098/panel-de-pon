using System.Collections;
using TMPro;
using UnityEngine;
namespace PanelDePon
{
    public class ReadySetGo : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI textComponent = null;
        [SerializeField] int totalSeconds = 4;
        private int currentSeconds = 0;
        private float ellapsedTime = 0;
        private void Awake()
        {
            GameManager.Pause();
            currentSeconds = totalSeconds;
        }
        private void Update()
        {
            if (currentSeconds > 0)
            {
                if (ellapsedTime < 1f) ellapsedTime += Time.deltaTime;
                else
                {
                    ellapsedTime = 0;
                    currentSeconds--;
                    UpdateTextUi();
                }
            }
        }

        private void UpdateTextUi()
        {
            if (currentSeconds == 0)
            {
                textComponent.text = "Go!";
                AudioManager.Instance.Play(AudioNames.lastCountDownAudio);
                StartCoroutine(DeactiveMe());
            }
            else
            {
                AudioManager.Instance.Play(AudioNames.countDownAudio);
                textComponent.text = currentSeconds.ToString();
            }
        }

        private IEnumerator DeactiveMe()
        {
            yield return new WaitForSeconds(1f);
            GameManager.Resume();
            gameObject.SetActive(false);
        }
    }
}