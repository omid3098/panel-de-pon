﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PanelDePon
{
    public class GameOverModal : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI team1TextComponent = null;
        [SerializeField] TextMeshProUGUI team2TextComponent = null;
        [SerializeField] ParticleSystem celebrationParticle = null;
        [SerializeField] int sceneIndex;
        private bool active;

        public void Show(int winnerTeamIndex)
        {
            AudioManager.Instance.Play(AudioNames.gameover);
            gameObject.SetActive(true);
            var teamCount = TeamManager.GetTeamsWithPlayerCount();
            if (teamCount != 1)
            {
                if (winnerTeamIndex == 2)
                {
                    team1TextComponent.text = "Win!";
                    team2TextComponent.text = "";
                    celebrationParticle.transform.SetParent(team1TextComponent.transform, false);
                }
                else
                {
                    team1TextComponent.text = "";
                    team2TextComponent.text = "Win!";
                    celebrationParticle.transform.SetParent(team2TextComponent.transform, false);
                }
                celebrationParticle.Play();
            }
            else
            {
                team1TextComponent.text = "";
                team2TextComponent.text = "";
            }
            StartCoroutine(ActiveAfterTwoSeconds());
        }

        private IEnumerator ActiveAfterTwoSeconds()
        {
            yield return new WaitForSeconds(2f);
            active = true;
        }

        private void Update()
        {
            if (active)
            {
                if (Input.anyKeyDown)
                {
                    TransitionManager.Instance.LoadScene(sceneIndex);
                    // SceneManager.LoadScene(sceneIndex);
                }
            }
        }
    }
}