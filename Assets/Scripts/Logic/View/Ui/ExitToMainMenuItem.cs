using UnityEngine;
using UnityEngine.SceneManagement;

namespace PanelDePon
{
    public class ExitToMainMenuItem : MenuItem
    {
        [SerializeField] int sceneIndex = 1;
        public override void Select()
        {
            TransitionManager.Instance.LoadScene(sceneIndex);
            // SceneManager.LoadScene(sceneIndex);
        }
    }
}