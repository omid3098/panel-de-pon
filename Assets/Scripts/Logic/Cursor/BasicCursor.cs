using UnityEngine;
namespace PanelDePon
{
    public class BasicCursor : ICursor
    {
        private GameObject viewPrefab;
        private Transform cursorTransform
        {
            get
            {
                try
                {
                    if (_cursorTransform == null || _cursorTransform.gameObject == null)
                        _cursorTransform = GameObject.Instantiate(viewPrefab).transform;
                    return _cursorTransform;
                }
                catch (System.Exception)
                {
                    Debug.LogError("Could not instantiate a new transform for current cursor");
                    throw;
                }
            }
        }
        private Transform _cursorTransform;
        public BasicCursor(GameObject viewPrefab)
        {
            this.viewPrefab = viewPrefab;
        }

        public int X { get; private set; }
        public int Y { get; private set; }

        public void UpdateView(Transform newParent)
        {
            cursorTransform.SetParent(newParent, false);
        }
        public void SetBoardPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public void Active() => cursorTransform.gameObject.SetActive(true);
        public void Deactive() => cursorTransform.gameObject.SetActive(false);

        public Transform CurrnetParent()
        {
            return cursorTransform.parent;
        }
    }
}
