using UnityEngine;
namespace PanelDePon
{
    public interface ICursor
    {
        int X { get; }
        int Y { get; }
        void UpdateView(Transform newParent);
        void SetBoardPosition(int x, int y);
        void Active();
        void Deactive();
        Transform CurrnetParent();
    }
}