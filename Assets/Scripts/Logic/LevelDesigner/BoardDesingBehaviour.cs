/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using UnityEngine;
namespace PanelDePon
{
    public class BoardDesingBehaviour : MonoBehaviour
    {
        /*
        ███████╗██╗███████╗██╗     ██████╗ ███████╗
        ██╔════╝██║██╔════╝██║     ██╔══██╗██╔════╝
        █████╗  ██║█████╗  ██║     ██║  ██║███████╗
        ██╔══╝  ██║██╔══╝  ██║     ██║  ██║╚════██║
        ██║     ██║███████╗███████╗██████╔╝███████║
        ╚═╝     ╚═╝╚══════╝╚══════╝╚═════╝ ╚══════╝
        */
        // [SerializeField] TextAsset rawBoardData = null;
        // [SerializeField] TileTheme theme = null;
        // public ISlotManager slotManager { get; set; }
        // public IInputManager inputManager { get; set; }
        // private ICursor cursor { get; set; }
        // private IPlayer[] players;
        public IBoard board;

        /*
         ██╗███╗   ██╗██╗████████╗██╗ █████╗ ██╗     ██╗███████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
         ██║████╗  ██║██║╚══██╔══╝██║██╔══██╗██║     ██║╚══███╔╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
         ██║██╔██╗ ██║██║   ██║   ██║███████║██║     ██║  ███╔╝ ███████║   ██║   ██║██║   ██║██╔██╗ ██║
         ██║██║╚██╗██║██║   ██║   ██║██╔══██║██║     ██║ ███╔╝  ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
         ██║██║ ╚████║██║   ██║   ██║██║  ██║███████╗██║███████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
         ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   ╚═╝╚═╝  ╚═╝╚══════╝╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
        */
        private void Awake()
        {

            // // Create Slot builder
            // slotManager = GetComponent<ISlotManager>();
            // // Create Input manager
            // ICombo combo = GetComponent<ICombo>();
            // players = GetComponentsInChildren<IPlayer>();
            // slotManager.Initialize();
            // board = new Board(slotManager, players, combo);

            // board.FillBoard(null);

            // BoardData boardData = null;
            // if (rawBoardData != null)
            // {
            //     var boardBytes = MessagePack.MessagePackSerializer.FromJson(rawBoardData.text);
            //     boardData = MessagePack.MessagePackSerializer.Deserialize<BoardData>(boardBytes);
            //     Debug.Log(boardData.slotsData[0, 0].tileData.tileName);
            // }

            // board.FillBoard(boardData);
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                FillBoardWithTileIndex(0);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                FillBoardWithTileIndex(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                FillBoardWithTileIndex(2);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                FillBoardWithGarbage(0);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                FillBoardWithGarbage(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                FillBoardWithGarbage(2);
            }
            board.Update();
        }

        private void FillBoardWithTileIndex(int tileIndex)
        {
            // var tile = theme.normalTiles[tileIndex];
            // slotManager.Fill(cursor.X, cursor.Y, TileBuilder.GetTile(tile.tileType, tile.tileName));
        }
        private void FillBoardWithGarbage(int tileIndex)
        {
            // var tile = theme.garbageTiles[tileIndex];
            // Debug.Log(tile.tileName);
            // slotManager.Fill(cursor.X, cursor.Y, TileBuilder.GetTile(tile.tileType, tile.tileName));
        }
    }
}