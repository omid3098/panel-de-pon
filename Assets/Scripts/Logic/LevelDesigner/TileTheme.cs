/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using UnityEngine;
namespace PanelDePon
{
    [CreateAssetMenu]
    public class TileTheme : ScriptableObject
    {
        public TileView[] normalTiles;
        public SingleGarbageTileView tilePrefab;
        public GarbageSpriteMaker team1Garbages;
        public GarbageSpriteMaker team2Garbages;
    }
}