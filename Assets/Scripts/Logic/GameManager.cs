/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */
using UnityEngine;

namespace PanelDePon
{
    public class GameManager : MonoBehaviour
    {
        public static event VoidDelegate OnPause;
        public static event VoidDelegate OnResume;
        public static float ellapsedTime { get; private set; }
        public static bool pause { get; private set; }
        private GameModeSetting setting = null;
        private int boardCount;
        private BoardConfigManager boardConfigManager;
        [SerializeField] BoardBehaviour boardPrefab = null;
        [SerializeField] float boardXPos = 7f;
        [SerializeField] GameOverModal gameOverModal = null;
        [SerializeField] PauseModal pauseMenu = null;
        [SerializeField] ReadySetGo readySetGo = null;
        [SerializeField] GameObject practiceModeObject = null;
        private BoardBehaviour team1Board = null;
        private BoardBehaviour team2Board = null;
        private void Awake()
        {
            TileBuilder.Instance.Initialize();
            team1Board = null;
            team2Board = null;
            var gamemode = GameModeManager.Instance.currentSetting.gameMode;
            if (gamemode == GameModes.casual)
            {
                AudioManager.Instance.Stop(AudioNames.CasualMenu);
                AudioManager.Instance.Play(AudioNames.CasualGame, true);
                AudioManager.Instance.Stop(AudioNames.ClassicGame);
                AudioManager.Instance.Stop(AudioNames.ClassicMenu);
            }
            else
            {
                AudioManager.Instance.Stop(AudioNames.CasualMenu);
                AudioManager.Instance.Stop(AudioNames.CasualGame);
                AudioManager.Instance.Play(AudioNames.ClassicGame, true);
                AudioManager.Instance.Stop(AudioNames.ClassicMenu);
            }
            boardConfigManager = Singleton<BoardConfigManager>.Instance;
            pause = false;
            ellapsedTime = 0;

            readySetGo.gameObject.SetActive(true);

            pauseMenu.gameObject.SetActive(false);
            pauseMenu.AddListeners();

            setting = GameModeManager.Instance.currentSetting;
            boardCount = TeamManager.GetTeamsWithPlayerCount();
            if (boardCount == 1)
            {
                var players = TeamManager.teams[0].players;
                if (players.Count == 0) players = TeamManager.teams[1].players;
                team1Board = Instantiate(boardPrefab);
                team1Board.Initialize(players.ToArray(), 0);
                team1Board.board.Attack += Attack;
                team1Board.board.GameOver += GameOver;

                practiceModeObject.SetActive(true);
            }
            else if (boardCount == 2)
            {
                var team1players = TeamManager.teams[0].players;
                team1Board = Instantiate(boardPrefab);
                team1Board.transform.position = new Vector3(-boardXPos, 0, 0);
                team1Board.Initialize(team1players.ToArray(), 0);
                team1Board.board.Attack += Attack;
                team1Board.board.GameOver += GameOver;

                var team2players = TeamManager.teams[1].players;
                team2Board = Instantiate(boardPrefab);
                team2Board.transform.position = new Vector3(boardXPos, 0, 0);
                team2Board.Initialize(team2players.ToArray(), 1);
                team2Board.board.Attack += Attack;
                team2Board.board.GameOver += GameOver;

                practiceModeObject.SetActive(false);
            }
            else
            {
                Debug.LogError("Invalid bordCount!" + boardCount);
            }
        }

        private void OnDisable()
        {
            pauseMenu.RemoveListeners();
            if (team1Board != null)
            {
                team1Board.board.Attack -= Attack;
                team1Board.board.GameOver -= GameOver;
                team1Board.Dispose();
            }
            if (team2Board != null)
            {
                team2Board.board.Attack -= Attack;
                team2Board.board.GameOver -= GameOver;
                team2Board.Dispose();
            }
        }

        private void GameOver(IBoard board)
        {
            pause = true;
            if (team1Board.board == board) gameOverModal.Show(1);
            else gameOverModal.Show(2);
        }

        private void Update()
        {
            if (pause == false) ellapsedTime += Time.deltaTime;
        }

        private void Attack(IBoard board, int attackStrenght, AttackType attackType)
        {
            if (boardCount > 1)
            {
                if (team1Board.board == board)
                    team2Board.board.DropGarbage(attackStrenght, attackType);
                else if (team2Board.board == board)
                    team1Board.board.DropGarbage(attackStrenght, attackType);
            }
        }

        internal static void Pause()
        {
            pause = true;
            if (OnPause != null) OnPause.Invoke();
        }
        internal static void Resume()
        {
            pause = false;
            if (OnResume != null) OnResume.Invoke();
        }
    }
}