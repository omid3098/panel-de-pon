﻿using UnityEngine;

namespace PanelDePon
{
    [CreateAssetMenu]
    public class GameModeSetting : ScriptableObject
    {
        public GameModes gameMode = GameModes.casual;
        public float FallDownDurationPerTile = 0.04f;
        public float SwapDuration = 0.1f;
        public float SingleTileDestroyWait = 0.2f;
        public float ExecutionWaitAfterMatch = 0.5f;
        public float FastModeSpeed = 2f;
        public float NormalModeSpeed = 0.02f;
        public float RecycleDurationPerTile = 0.1f;
        public float EllapsedTimeSpeedEffect = 5000f; // Higher value => lower effect by ellapsed time
        public TileTheme theme;
    }
}