/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using MessagePack;
using UnityEngine;

namespace PanelDePon
{
    public class BoardSaver : MonoBehaviour
    {
        [SerializeField] BoardDesingBehaviour boardBehaviour = null;
        private BoardData boardData;
        private void Awake()
        {
            boardData = new BoardData();
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                int widthCount = boardBehaviour.board.slotManager.widthCount;
                int heightCount = boardBehaviour.board.slotManager.heightCount;
                boardData.slotsData = new SlotData[widthCount, heightCount];
                for (int i = 0; i < widthCount; i++)
                {
                    for (int j = 0; j < heightCount; j++)
                    {
                        // boardData.slotsData[i, j] = boardBehaviour.board.slotManager.GetSlot(i, j).data;
                    }
                }
                var bytes = MessagePackSerializer.Serialize(boardData);
                string message = MessagePackSerializer.ToJson(bytes);
                Debug.Log(message);
                message.CopyToClipboard();
            }
        }
    }
    public static class ClipboardExtension
    {
        /// <summary>
        /// Puts the string into the Clipboard.
        /// </summary>
        /// <param name="str"></param>
        public static void CopyToClipboard(this string str)
        {
            var textEditor = new TextEditor();
            textEditor.text = str;
            textEditor.SelectAll();
            textEditor.Copy();
        }
    }
}