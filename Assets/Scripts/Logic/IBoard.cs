namespace PanelDePon
{
    public interface IBoard
    {
        event AttackDelegate Attack;
        event BoardDelegate GameOver;
        ISlotManager slotManager { get; }
        void FillBoard(BoardData boardData);
        void Update();
        void DropGarbage(int attackStrenght, AttackType attackType);
    }
}