﻿/*
 
 ██████╗  █████╗ ███╗   ██╗███████╗██╗     ██████╗ ███████╗██████╗  ██████╗ ███╗   ██╗
 ██╔══██╗██╔══██╗████╗  ██║██╔════╝██║     ██╔══██╗██╔════╝██╔══██╗██╔═══██╗████╗  ██║
 ██████╔╝███████║██╔██╗ ██║█████╗  ██║     ██║  ██║█████╗  ██████╔╝██║   ██║██╔██╗ ██║
 ██╔═══╝ ██╔══██║██║╚██╗██║██╔══╝  ██║     ██║  ██║██╔══╝  ██╔═══╝ ██║   ██║██║╚██╗██║
 ██║     ██║  ██║██║ ╚████║███████╗███████╗██████╔╝███████╗██║     ╚██████╔╝██║ ╚████║
 ╚═╝     ╚═╝  ╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝╚═════╝ ╚══════╝╚═╝      ╚═════╝ ╚═╝  ╚═══╝
                                                                                      
 * File Created: Friday, 9th August 2019 11:40:47 am
 * Author: Omid Saadat (info@omid-saadat.com)
 * -----
 * Last Modified: Friday, 9th August 2019 11:52:06 am
 * Modified By: Omid Saadat (info@omid-saadat.com>)
 * -----
 * Copyright 2019 - Omid Saadat
 */

using DG.Tweening;
using UnityEngine;
namespace PanelDePon
{
    public delegate void VoidDelegate();
    public delegate void IntDelegate(int value);
    public delegate void AttackDelegate(IBoard board, int attackStrenght, AttackType attackType);
    public delegate void BoardDelegate(IBoard board);
    public delegate void GarbageCleanDelegate(ISlot slot, int width, int height);
    public delegate void SlotDelegate(ISlot slot);
    public delegate void TileDelegate(ITile tile);
    public delegate void ComboDelegate(Transform highestSlot, int combo);
    public delegate void BoolDelegate(bool value);
    public delegate void GarbageDelegate(int width, int height);
    public class BoardBehaviour : MonoBehaviour, IBoardBehaviour
    {      /*
        ███████╗██╗███████╗██╗     ██████╗ ███████╗
        ██╔════╝██║██╔════╝██║     ██╔══██╗██╔════╝
        █████╗  ██║█████╗  ██║     ██║  ██║███████╗
        ██╔══╝  ██║██╔══╝  ██║     ██║  ██║╚════██║
        ██║     ██║███████╗███████╗██████╔╝███████║
        ╚═╝     ╚═╝╚══════╝╚══════╝╚═════╝ ╚══════╝
        */
        [SerializeField] SpriteRenderer[] boardFrames = null;
        [SerializeField] SpriteRenderer backDark = null;
        [SerializeField] GameObject boardMask = null;
        [SerializeField] float shakeDuration = 0.1f;
        [SerializeField] GameObject octoDecors = null;
        [SerializeField] GameObject snailDecors = null;

        private IPlayer[] playerArray;
        private Tweener shakeTween;

        public ISlotManager slotManager { get; set; }

        private Shaker shaker;

        public IBoard board { get; private set; }

        /*
         ██╗███╗   ██╗██╗████████╗██╗ █████╗ ██╗     ██╗███████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
         ██║████╗  ██║██║╚══██╔══╝██║██╔══██╗██║     ██║╚══███╔╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
         ██║██╔██╗ ██║██║   ██║   ██║███████║██║     ██║  ███╔╝ ███████║   ██║   ██║██║   ██║██╔██╗ ██║
         ██║██║╚██╗██║██║   ██║   ██║██╔══██║██║     ██║ ███╔╝  ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
         ██║██║ ╚████║██║   ██║   ██║██║  ██║███████╗██║███████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
         ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   ╚═╝╚═╝  ╚═╝╚══════╝╚═╝╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
        */
        public void Initialize(IPlayer[] players, int teamIndex)
        {
            this.playerArray = players;

            // Create Slot builder
            slotManager = GetComponent<ISlotManager>();
            shaker = GetComponent<Shaker>();

            // Create Input manager
            ICombo combo = GetComponent<ICombo>();
            slotManager.Initialize(teamIndex, players);
            if (teamIndex == 0)
            {
                octoDecors.SetActive(false);
                snailDecors.SetActive(true);
            }
            else
            {
                octoDecors.SetActive(true);
                snailDecors.SetActive(false);
            }
            var newBoardSize = new Vector2(slotManager.widthCount + 1.3f, slotManager.heightCount + 1f);
            foreach (var boardFrame in boardFrames) boardFrame.size = newBoardSize;
            backDark.size = newBoardSize;
            boardMask.transform.localScale = new Vector3(slotManager.widthCount, slotManager.heightCount);

            board = new Board(slotManager, combo, teamIndex);

            board.FillBoard(null);
        }
        private void Update() { board.Update(); }
        public void Shake()
        {
            if (shaker.busy == false)
            {
                AudioManager.Instance.Play(AudioNames.dropGarbage);
                shaker.Shake();
            }
            // if (shakeTween == null || shakeTween.IsPlaying() == false)
            // {
            //     AudioManager.Instance.Play(AudioNames.dropGarbage);
            //     transform.localScale = new Vector3(0.8f, 0.8f, 1f);
            //     shakeTween = transform.DOScale(Vector3.one, shakeDuration);
            //     // shakeTween = transform.DOShakeScale(shakeDuration, shakeStrenght);
            //     shakeTween.SetEase(Ease.OutElastic);
            //     shakeTween.onComplete += () =>
            //     {
            //         transform.localScale = Vector3.one;
            //     };
            // }
        }
        public void Dispose()
        {
            slotManager = null;
            board = null;
            playerArray = null;
        }
    }

    internal interface IBoardBehaviour
    {
        IBoard board { get; }
        void Initialize(IPlayer[] players, int teamIndex);
        void Shake();
        void Dispose();
    }
}