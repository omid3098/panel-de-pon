using UnityEngine;

namespace OpenTween.Components
{
    public class MoveTransition : BaseTransitionTween<Vector3>
    {
        [SerializeField] Vector3 targetPosition = Vector3.zero;
        public override void Complete() => transform.position = targetValue;
        protected override void OnUpdate() => transform.position = tween.currentValue;
        protected override void InitTransitionFrom()
        {
            startValue = relative ? transform.position + targetPosition : targetPosition;
            targetValue = transform.position;
        }

        protected override void InitTransitionTo()
        {
            startValue = transform.position;
            targetValue = relative ? transform.position + targetPosition : targetPosition;
        }

        protected override Tween<Vector3> CreateTweener()
        {
            var tween = new Vector3Tween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        public override void Discard()
        {
            transform.position = startValue;
        }
    }
}