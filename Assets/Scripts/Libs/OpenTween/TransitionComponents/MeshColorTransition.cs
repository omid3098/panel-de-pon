using UnityEngine;

namespace OpenTween.Components
{
    public class MeshColorTransition : BaseTransitionTween<Color>
    {
        [SerializeField] Renderer _renderer = null;
        [SerializeField] Color targetColor = Color.black;

        protected override void OnUpdate()
        {
            _renderer.material.color = tween.currentValue;
        }

        protected override void InitTransitionFrom()
        {
            startValue = relative ? _renderer.material.color + targetColor : targetColor;
            targetValue = _renderer.material.color;
        }

        protected override void InitTransitionTo()
        {
            startValue = _renderer.material.color;
            targetValue = relative ? _renderer.material.color + targetColor : targetColor;
        }

        public override void Complete()
        {
            _renderer.material.color = targetValue;
        }

        protected override Tween<Color> CreateTweener()
        {
            var tween = new ColorTween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        public override void Discard()
        {
            _renderer.material.color = startValue;
        }
    }
}