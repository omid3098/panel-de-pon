using UnityEngine;
using UnityEngine.UI;

namespace OpenTween.Components
{
    public class ImageAlphaTransition : BaseTransitionTween<float>
    {
        [SerializeField] Image image = null;
        [SerializeField] float targetAlpha = 0;
        protected override void OnUpdate()
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, tween.currentValue);
        }

        protected override void InitTransitionFrom()
        {
            float relativeMax = image.color.a + targetAlpha;
            startValue = relative ? (relativeMax > 1 ? 1 : relativeMax) : targetAlpha;
            targetValue = image.color.a;
        }

        protected override void InitTransitionTo()
        {
            startValue = image.color.a;
            float relativeMax = image.color.a + targetAlpha;
            targetValue = relative ? (relativeMax > 1 ? 1 : relativeMax) : targetAlpha;
        }

        public override void Complete()
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, targetValue);
        }

        protected override Tween<float> CreateTweener()
        {
            var tween = new FloatTween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        public override void Discard()
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, startValue);
        }
    }
}