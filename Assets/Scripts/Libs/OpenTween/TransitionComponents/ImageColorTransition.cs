using UnityEngine;
using UnityEngine.UI;

namespace OpenTween.Components
{
    public class ImageColorTransition : BaseTransitionTween<Color>
    {
        [SerializeField] Image image = null;
        [SerializeField] Color targetColor = Color.black;

        protected override void OnUpdate()
        {
            image.color = tween.currentValue;
        }

        protected override void InitTransitionFrom()
        {
            startValue = relative ? image.color + targetColor : targetColor;
            targetValue = image.color;
        }

        protected override void InitTransitionTo()
        {
            startValue = image.color;
            targetValue = relative ? image.color + targetColor : targetColor;
        }

        public override void Complete()
        {
            image.color = targetValue;
        }

        protected override Tween<Color> CreateTweener()
        {
            var tween = new ColorTween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        public override void Discard()
        {
            image.color = startValue;
        }
    }
}