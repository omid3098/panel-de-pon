using UnityEngine;

namespace OpenTween.Components
{
    public abstract class BaseTransitionTween<T> : MonoBehaviour where T : struct
    {
        public enum TransitionType
        {
            To,
            From,
        }
        [SerializeField] bool playOnEnable = true;
        [SerializeField] bool discardOnDisable = true;
        [SerializeField] protected TweenCurve tweenCurve;
        [SerializeField] protected float duration = 0.4f;
        [SerializeField] protected float delay = 0f;
        [SerializeField] protected TransitionType transitionType = TransitionType.To;
        [SerializeField] protected bool timeScale = true;
        [SerializeField] protected bool relative;
        [Range(-1, 1000000)] [SerializeField] int loopCount = 1;
        [SerializeField] LoopType loopType = LoopType.Restart;
        private int ellapsedLoops = 0;
        protected Tween<T> tween;
        protected T startValue;
        protected T targetValue;
        private void Awake()
        {
            if (transitionType == TransitionType.To) InitTransitionTo();
            else InitTransitionFrom();
            tween = CreateTweener().SetLoops(loopCount, loopType);
        }

        protected abstract Tween<T> CreateTweener();

        private void OnEnable()
        {
            if (playOnEnable)
            {
                Play();
            }
        }
        private void OnDisable()
        {
            if (discardOnDisable) Discard();
        }


        protected void Play()
        {
            tween.SetDelay(delay);
            tween.Play();
        }

        protected abstract void InitTransitionFrom();
        protected abstract void InitTransitionTo();
        protected abstract void OnUpdate();
        public abstract void Complete();
        public abstract void Discard();
        private void Update()
        {
            tween.Update();
            if (tween.isPlaying) OnUpdate();

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Play();
            }
        }
    }
}