using UnityEngine;

namespace OpenTween.Components
{
    public class ScaleTransition : BaseTransitionTween<Vector3>
    {
        [SerializeField] Vector3 targetScale = Vector3.zero;
        public override void Complete()
        {
            transform.localScale = targetValue;
        }

        public override void Discard()
        {
            transform.localScale = startValue;
        }

        protected override Tween<Vector3> CreateTweener()
        {
            var tween = new Vector3Tween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        protected override void InitTransitionFrom()
        {
            startValue = relative ? transform.localScale + targetScale : targetScale;
            targetValue = transform.localScale;
        }

        protected override void InitTransitionTo()
        {
            startValue = transform.localScale;
            targetValue = relative ? transform.localScale + targetScale : targetScale;
        }

        protected override void OnUpdate()
        {
            transform.localScale = tween.currentValue;
        }
    }
}