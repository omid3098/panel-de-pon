using UnityEngine;

namespace OpenTween.Components
{
    public class MeshAlphaTransition : BaseTransitionTween<float>
    {
        [SerializeField] Renderer _renderer = null;
        [SerializeField] float targetAlpha = 0;
        protected override void OnUpdate()
        {
            _renderer.material.color = new Color(_renderer.material.color.r, _renderer.material.color.g, _renderer.material.color.b, tween.currentValue);
        }

        protected override void InitTransitionFrom()
        {
            float relativeMax = _renderer.material.color.a + targetAlpha;
            startValue = relative ? (relativeMax > 1 ? 1 : relativeMax) : targetAlpha;
            targetValue = _renderer.material.color.a;
        }

        protected override void InitTransitionTo()
        {
            startValue = _renderer.material.color.a;
            float relativeMax = _renderer.material.color.a + targetAlpha;
            targetValue = relative ? (relativeMax > 1 ? 1 : relativeMax) : targetAlpha;
        }

        public override void Complete()
        {
            _renderer.material.color = new Color(_renderer.material.color.r, _renderer.material.color.g, _renderer.material.color.b, targetValue);
        }

        protected override Tween<float> CreateTweener()
        {
            var tween = new FloatTween(startValue, targetValue, duration, tweenCurve);
            tween.timeScale = timeScale;
            return tween;
        }

        public override void Discard()
        {
            _renderer.material.color = new Color(_renderer.material.color.r, _renderer.material.color.g, _renderer.material.color.b, startValue);
        }
    }
}