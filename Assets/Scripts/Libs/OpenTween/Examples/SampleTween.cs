﻿using OpenTween;
using UnityEngine;

public class SampleTween : MonoBehaviour
{
    private FloatTween tween;

    private void Awake()
    {
        tween = new FloatTween(1, 5, 0.5f);
        tween.OnUpdate += (value) =>
        {
            Debug.Log("Updating tween!");
            transform.position = new Vector3(value, transform.position.y, transform.position.z);
        };
        tween.OnComplete += () => { Debug.Log("Complete!"); };
        tween.SetLoops(-1, LoopType.Yoyo);
        tween.Play();
    }

    private void Update()
    {
        tween.Update();
    }
}
