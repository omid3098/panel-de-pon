﻿using UnityEngine;
namespace OpenTween
{
    [CreateAssetMenu]
    public class TweenCurve : ScriptableObject
    {
        public AnimationCurve curve;
    }
}