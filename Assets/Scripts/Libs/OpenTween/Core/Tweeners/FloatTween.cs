using UnityEngine;

namespace OpenTween
{
    public class FloatTween : Tween<float>
    {
        public FloatTween(float startValue, float targetValue, float duration) : base(startValue, targetValue, duration) { }
        public FloatTween(float startValue, float targetValue, float duration, TweenCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        public FloatTween(float startValue, float targetValue, float duration, AnimationCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        protected override float CalculateCurvedLerp(float curveValue, float lerpTime)
        {
            float currentFloat = Mathf.Lerp(startValue, targetValue, lerpTime);
            // apply curve to lerped value
            currentFloat += (targetValue - startValue) * (curveValue - lerpTime);
            return currentFloat;
        }
    }
}