using UnityEngine;

namespace OpenTween
{
    public class ColorTween : Tween<Color>
    {
        public ColorTween(Color startValue, Color targetValue, float duration) : base(startValue, targetValue, duration) { }
        public ColorTween(Color startValue, Color targetValue, float duration, TweenCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        public ColorTween(Color startValue, Color targetValue, float duration, AnimationCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        protected override Color CalculateCurvedLerp(float curveValue, float lerpTime)
        {
            Color currentFloat = Color.Lerp(startValue, targetValue, lerpTime);
            // apply curve to lerped value
            currentFloat += (targetValue - startValue) * (curveValue - lerpTime);
            return currentFloat;
        }
    }
}