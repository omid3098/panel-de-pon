using UnityEngine;

namespace OpenTween
{
    public class Vector3Tween : Tween<Vector3>
    {
        public Vector3Tween(Vector3 startValue, Vector3 targetValue, float duration) : base(startValue, targetValue, duration) { }
        public Vector3Tween(Vector3 startValue, Vector3 targetValue, float duration, TweenCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        public Vector3Tween(Vector3 startValue, Vector3 targetValue, float duration, AnimationCurve tweenCurve) : base(startValue, targetValue, duration, tweenCurve) { }
        protected override Vector3 CalculateCurvedLerp(float curveValue, float lerpTime)
        {
            Vector3 currentFloat = Vector3.Lerp(startValue, targetValue, lerpTime);
            // apply curve to lerped value
            currentFloat += (targetValue - startValue) * (curveValue - lerpTime);
            return currentFloat;
        }
    }
}