﻿using System.Threading.Tasks;
using UnityEngine;

namespace OpenTween
{
    public enum LoopType
    {
        Restart,
        Yoyo,
    }
    // TODO we need to manually update tweens for now. it would be cool 
    public abstract class Tween<T> where T : struct
    {
        public delegate void TweenDelegate(T value);
        public delegate void VoidDelegate();
        // tween events:
        public event TweenDelegate OnUpdate;
        public event VoidDelegate OnComplete;

        // check if tween is already playing? on pause and stop, isplayin will be false.
        public bool isPlaying { get; private set; }

        // we store start, end and current values to be able to roll back or instantly finish tween
        protected T startValue { get; private set; }
        protected T targetValue { get; private set; }
        public T currentValue { get; private set; }

        protected T initialStartValue { get; private set; }
        protected T initialTargetValue { get; private set; }

        private float delayEllapsedTime;


        // how much time is ellapsed from the start of Play method? this value will not reset after each loop
        protected float totalEllapsedTime { get; private set; }

        // ellapsed time for each loop
        protected float loopEllapsedTime { get; private set; }

        // tween duration for each loop
        protected float duration { get; set; }

        // tween duration for all loop counts
        protected float totalDuration { get; set; }

        // what animation curve is tween using? default value will tween linear.
        // you can also make TweenCurve scriptableobject and pass its curve to any tween to be able to reuse your curves
        // some premade curves are provided with the package
        protected AnimationCurve tweenCurve { get; set; }

        // if timescale is true, your tweens will be freez if you set Time.timeScale = 0
        public bool timeScale { get; set; }
        float delay { get; set; }

        // how many loops this tween should go?
        int loopCount = 1;

        // at the end of each loop, reverse the path?(like Yoyo) or start from begining again?(Restart)
        LoopType loopType;

        public Tween(T startValue, T targetValue, float duration)
        {
            Initialize(startValue, targetValue, duration);
        }

        private void Initialize(T startValue, T targetValue, float duration)
        {
            this.startValue = startValue;
            this.targetValue = targetValue;
            this.duration = duration;
            initialStartValue = startValue;
            initialTargetValue = targetValue;
            loopCount = 1;
            totalDuration = duration;
            timeScale = true;
            delay = 0;
        }

        public Tween(T startValue, T targetValue, float duration, TweenCurve tweenCurve)
        {
            Initialize(startValue, targetValue, duration);
            SetCurve(tweenCurve.curve);
        }

        public Tween(T startValue, T targetValue, float duration, AnimationCurve animationCurve)
        {
            Initialize(startValue, targetValue, duration);
            SetCurve(animationCurve);
        }

        public Tween<T> SetCurve(AnimationCurve tweenCurve)
        {
            this.tweenCurve = tweenCurve;
            return this;
        }
        public Tween<T> SetDelay(float delay)
        {
            this.delay = delay;
            return this;
        }
        public Tween<T> SetLoops(int loopCount, LoopType loopType = LoopType.Restart)
        {
            this.loopCount = loopCount;
            this.loopType = loopType;
            totalDuration = duration * loopCount;
            // switch (loopType)
            // {
            //     case LoopType.Restart: this.tweenCurve.postWrapMode = WrapMode.Loop; break;
            //     case LoopType.Yoyo: this.tweenCurve.postWrapMode = WrapMode.PingPong; break;
            //     default: break;
            // }
            return this;
        }
        public void Play()
        {
            delayEllapsedTime = 0;
            totalEllapsedTime = 0;
            loopEllapsedTime = 0;
            startValue = initialStartValue;
            targetValue = initialTargetValue;
        }

        public void Pause() => isPlaying = false;
        public void Resume() => isPlaying = true;

        internal void Update()
        {
            if (delayEllapsedTime <= delay)
            {
                delayEllapsedTime += timeScale ? Time.deltaTime : Time.unscaledDeltaTime;
            }
            else
            {
                isPlaying = true;
            }
            if (isPlaying)
            {
                float lerpTime = loopEllapsedTime / duration;
                // curve value must have the same value as lerpTime to use liniar if no curve is provided
                float curveValue = lerpTime;
                if (tweenCurve != null) curveValue = tweenCurve.Evaluate(lerpTime);
                currentValue = CalculateCurvedLerp(curveValue, lerpTime);

                if (OnUpdate != null) OnUpdate(currentValue);
                float deltaTime = timeScale ? Time.deltaTime : Time.unscaledDeltaTime;
                totalEllapsedTime += deltaTime;
                loopEllapsedTime += deltaTime;
                if (totalEllapsedTime > totalDuration && loopCount != -1) Complete();

                // How to calculate path for multiple looptypes:

                // target value |     /\          /\
                //              |    /  \        /  \
                //              |   /    \      /    \
                //              |  /      \    /      \
                //              | /        \  /        \
                // start value  |/__________\/__________\_
                //                                         loop ellapsed time

                //  first we detect that if one loop has been passed:
                if (loopEllapsedTime > duration)
                {
                    //  if we have a Yoyo looptype, we just need to swap start value and end value without changing loop ellapsed time
                    if (loopType == LoopType.Yoyo)
                    {
                        var tmp = startValue;
                        startValue = targetValue;
                        targetValue = tmp;
                    }
                    // and if the looptype is restart, we simply set loop ellapsed time to 0
                    loopEllapsedTime = 0;
                }
            }
        }
        protected abstract T CalculateCurvedLerp(float curveValue, float lerpTime);
        private void Complete()
        {
            isPlaying = false;
            if (OnComplete != null) OnComplete();
        }
    }
}